#pragma once

#include<queue>
#include<experimental/optional>
#include<movement_request.h>
#include <movement_instruction.h>

template<class T>
using optional = std::experimental::optional<T>;


/**
 * @brief A class to hold motions for a motor.
 *
 * It allows to enqueue motion requests and to override the current motions with a new request.
 * It will ensure, that an emergency motion is executed whenever the queue runs empty while the motor
 * is still moving.
 * That emergency motion will move the motor to the last destination and stop it there with the movement
 * characeristics (max_vel and accelerations) of the last movement.
 */
template<class Motion>
class MotionQueue {
public:
    using time_point = typename Motion::time_point;

    /**
     * @brief Request a movement for the motor to be executed after all the other motions are done.
     *
     * current_pos and now are only needed when the queue is empty. Otherwise, the start position and time is derived
     * from the last motion.
     *
     * @param movement_request The movement to request.
     * @param current_pos The current position of the motor.
     * @param now The current time.
     * @return The motion that resulted from the movement request.
     */
    auto request_movement(const MovementRequest& movement_request, int current_pos, time_point now) {
        auto motion = motions_.empty() ?
                      compute_motion(movement_request, current_pos, 0, now) :
                      compute_motion(movement_request, motions_.back());
        motions_.push(motion);
        update_emergency_motion();
        return motion;
    }

    /**
     * @brief Request a movement for the motor to be executed immediately. All other movement request in the queue are
     * discarded.
     *
     * The current_pos is only needed for the case that the queue is empty, otherwise the start position is derived
     * from the current motion.
     *
     * @param movement_request The movement to request.
     * @param current_pos The current position of the motor.
     * @param now The current time.
     * @return The motion that resulted from the movement request.
     */
    auto request_movement_now(const MovementRequest& movement_request, int current_pos, time_point now) {
        if(motions_.empty()) {
            auto motion = compute_motion(movement_request, current_pos, 0, now);
            motions_.push(motion);
        } else {
            auto current_motion = motions_.front();
            auto motion = compute_motion(
                    movement_request,
                    current_motion(now),
                    current_motion.get_velocity(now),
                    now);
            motions_ = std::queue<Motion>(); // clear the queue
            motions_.push(motion);
        }
        update_emergency_motion();
        return motions_.back();
    }

    /**
     * @brief Advance the queue up to a given point in time.
     *
     * This will pop motions from the queue that are no longer needed since they ended before the given time.
     *
     * @param now The current time.
     * @return The motion at the front of the queue or the emergency motion if the queue is empty.
     */
    optional<Motion> advance_to(typename Motion::time_point now) {
        pop_outdated_motions(now);

        if(motions_.empty()) {
            return emergency_motion_;
        } else {
            return motions_.front();
        }
    }

    /**
     * @brief Returns the time when the last motion finished.
     *
     * @param now The current time (will be returned back if the queue is empty).
     */
    auto get_last_motion_end_time(time_point now) const {
        if(motions_.empty()) {
            return now;
        } else {
            return motions_.back().end_time();
        }
    }

    auto get_num_enqueued_motions() const {
        return motions_.size();
    }

private:
    std::queue<Motion> motions_;
    optional<Motion> emergency_motion_;

    void pop_outdated_motions(typename Motion::time_point now) {
        while(not motions_.empty() and motions_.front().end_time() < now) {
            motions_.pop();
        }
        if(emergency_motion_ and emergency_motion_->end_time() < now) {
            emergency_motion_ = {};
        }
    }

    void update_emergency_motion() {
        if(not motions_.empty() and motions_.back().relative_motion_.final_velocity != 0.0) {
            auto last_motion = motions_.back();
            auto celeration = std::max(std::abs(last_motion.relative_motion_.instruction.a1), last_motion.relative_motion_.instruction.a3);
            auto start_vel = last_motion.relative_motion_.final_velocity;
            auto instruction = compute_movement_instruction(
                    0,
                    start_vel,
                    0,
                    celeration,
                    celeration,
                    start_vel,
                    0);
            emergency_motion_ = Motion{{start_vel, std::move(instruction)}, last_motion.destination(), last_motion.end_time()};
        }
    }

    auto compute_motion(const MovementRequest& movement_request, double current_pos, double current_vel, typename Motion::time_point start_time) -> Motion {
        auto instruction = compute_movement_instruction(
                movement_request.destination - current_pos,
                current_vel,
                movement_request.end_velocity,
                movement_request.acceleration,
                movement_request.deceleration,
                movement_request.max_velocity,
                movement_request.duration);
        return Motion{{current_vel, std::move(instruction)}, current_pos, start_time};
    }

    auto compute_motion(const MovementRequest& movement_request, const Motion& previous_motion) -> Motion {
        return compute_motion(
                movement_request,
                previous_motion.destination(),
                previous_motion.relative_motion_.final_velocity,
                previous_motion.end_time());
    }
};