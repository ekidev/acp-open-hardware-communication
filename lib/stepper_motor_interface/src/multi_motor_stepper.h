#pragma once

#include<array>
#include<dmux_interface.h>

/**
 * @brief A task to generate steps that move stepper motors behind a dmux interface to their target positions.
 * 
 * The dmux interface can only perform steps with four motors at a time. 
 * This class helps with performing those steps in 4-motor-batches.
 * 
 * Set the public `target_positions_` member to modify where motors should move to and
 * call the `loop()` method as often as possible to ensure the required steps are performed via the dmux interface.
 * 
 * @note If you change the target postions faster than the motor can move or accelerate 
 *  due to physical constraints, steps will most probably be lost.
 * 
 * @note Each call to `loop()` will only ever execute one step per motor. 
 *  If you change the target positions between two calls to `loop()` by a value of more than 1, future calls to `loop()` will catch up on this difference.
 *  Those delayed steps however are counted.
 * 
 * @tparam NUM_MOTORS The total number of motors to generate steps for.
 */
template<uint8_t NUM_MOTORS>
class MultiMotorStepper {
public:
    std::array<int, NUM_MOTORS> target_positions_ = {};

    /**
     * @brief Generates steps motor steps to make them move towards their target positions.
     * 
     * Each call to loop() will execute at most one step per motor. 
     * The frequency at wich you call this method determines the max speed of your motors.
     */
    void loop() {
        // Note: this loop never addresses motors beyond NUM_MOTORS even when NUM_MOTORS is not a multiple of four since
        // calc_step as well as register_step do a range-check.
        for(uint8_t i = 0, address = 0; i < NUM_MOTORS; i += 4, address++) {
            dmux_interface_.select_quartet(address);

            auto step1 = calc_step(i);
            auto step2 = calc_step(i+1);
            auto step3 = calc_step(i+2);
            auto step4 = calc_step(i+3);

            dmux_interface_.do_step_with_all_motors_of_quartet(
                step1 > 0,
                step1 != 0,

                step2 > 0,
                step2 != 0,

                step3 > 0,
                step3 != 0,

                step4 > 0,
                step4 != 0    
                );

            register_step(i, step1);
            register_step(i+1, step2);
            register_step(i+2, step3);
            register_step(i+3, step4);
        }
    }

    /**
     * @brief Get the current position of a motor.
     * 
     * @note There are no range checks on this function. If the `idx` is larger than the number of motors you access random parts of the memory.
     */
    int get_current_position(size_t idx) const { return current_positions_[idx]; }

    /**
     * @brief Returns the number of steps that were delayed since the target position changed faster than the motor could move.
     *
     * Resets the number of delayed steps.
     * 
     * @note If this value is non-zero, that is an indicator, that the computational load is too high for the speed at which you are driving your motors.
     */
    int collect_delayed_steps() {
        auto steps = delayed_steps_;
        delayed_steps_ = 0;
        return steps;
    }

private:
    DmuxInterface dmux_interface_;
    std::array<int, NUM_MOTORS> current_positions_ = {};
    size_t delayed_steps_ = 0;

    /**
     * @brief Calculates the steps, that should be executed now for a given motor.
     * 
     * @param idx The index of tho motor, range checks are applied.
     * @return -1, if the motor should move backwards, 0 if the motor should not move, 1 if the motor should move forwards.
     */
    auto calc_step(size_t idx) {
        if(idx >= NUM_MOTORS) {
            return 0;
        }
        auto step = target_positions_[idx] - current_positions_[idx];
        if(step > 1) {
            delayed_steps_++;
            return 1;
        }
        if(step < -1) {
            delayed_steps_++;
            return -1;
        }
        return step;
    }

    /**
     * @brief Register a step, that has been performed using the dmux interface and update the current motor positions.
     * 
     * @param idx The index of the motor that has been moved. Range checks are applied.
     * @param step 1 for a forward step, -1 for a backward step, 0 for no step.
     */
    auto register_step(size_t idx, int step) {
        if(idx < NUM_MOTORS) {
            current_positions_[idx] += step;
        }
    }
};