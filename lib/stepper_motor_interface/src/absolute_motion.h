#pragma once

#include<chrono>

/**
 * @brief Describes a motor motion between to points in time and two positions of the motor.
 * 
 * @tparam RelativeMotionType The type of motion to describe. Right now this could be an `AccelDecelMotion` or a `ConstantSpeedMotion`.
 * @tparam Clock The kind of clock to use to track the time. The teensy_clock is used by default.
 */
template<class RelativeMotionType, class Clock>
struct AbsoluteMotion {
    using time_point = typename Clock::time_point;
    using duration = typename Clock::duration;

    RelativeMotionType relative_motion_;
    double origin_;
    time_point start_time_;

    /**
     * @brief Computes the position of the motor at a given point in time.
     */
    auto operator() (time_point t) const {
        double t_ = std::chrono::duration<double>(t - start_time_).count();
        return relative_motion_(t_) + origin_;
    }

    /**
     * @brief Compute the velocity of the motor at a given point in time.
     */
    auto get_velocity(time_point t) const {
        double t_ = std::chrono::duration<double>(t - start_time_).count();
        return relative_motion_.get_velocity(t_);
    }

    /**
     * @brief compute the absolute destination of the motion.
     */
    auto destination() const {
        return relative_motion_.destination + origin_;
    }

    /**
     * @brief Computes the absolute end time of the motion.
     */
    auto end_time() const {  // TODO: make this pre-computed and not a function
        auto d = std::chrono::duration<double>(relative_motion_.duration);
        return std::chrono::time_point_cast<duration>(start_time_ + d);
    }
};