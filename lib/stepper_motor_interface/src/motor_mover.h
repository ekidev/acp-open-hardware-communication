//
// Created by m on 8/19/22.
//

#pragma once

#include<multi_motor_stepper.h>
#include<motion_queue.h>
#include<accel_decel_motion.h>
#include<absolute_motion.h>

/**
 * @brief A class to fulfill movement requests.
 *
 * It allows to enqueue motion requests and to override the current motions with a new request.
 * It will ensure, that an emergency motion is executed whenever the queue runs empty while the motor.
 * That emergency motion will move the motor to the last destination and stop it there with the movement.
 *
 * It uses a MultiMotorStepper to move the motors.
 *
 * @tparam NUM_MOTORS The number of motors to use.
 * @tparam Motion The motion type to use.
 */
template<uint8_t NUM_MOTORS, class Motion = AbsoluteMotion<AccelDecelMotion, teensy_clock>>
class MotorMover {
public:
    using time_point = typename Motion::time_point;

    /**
     * @brief Executes motor steps according to the enqueued motions.
     *
     * This method should be called periodically to ensure stepper steps are executed.
     * Preferably from a timer interrupt.
     *
     * @param now The current time.
     */
    void loop(time_point now) {
        for(size_t i = 0; i < motion_queues_.size(); ++i) {
            auto current_motion = motion_queues_[i].advance_to(now);
            if(current_motion) {
                motor_stepper_.target_positions_[i] = (*current_motion)(now);
            }
        }

        motor_stepper_.loop();
    }

    /**
     * @brief Enqueues a motion request for a given motor.
     *
     *
     * @param movement_request The motion to request.
     * @param motor_idx The motor to move.
     * @param now The current time.
     * @return The motion that resulted from the movement request.
     */
    auto request_movement(const MovementRequest& movement_request, size_t motor_idx, time_point now) {
        return motion_queues_[motor_idx].request_movement(
                movement_request,
                motor_stepper_.get_current_position(motor_idx),
                now);
    }

    /**
     * @brief Overrides the current motion queue with a given movement request.
     *
     * @param movement_request The motion to request.
     * @param motor_idx The motor to move.
     * @param now The current time.
     * @return The motion that resulted from the movement request.
     */
    auto request_movement_now(const MovementRequest& movement_request, size_t motor_idx, time_point now) {
        return motion_queues_[motor_idx].request_movement_now(
                movement_request, motor_stepper_.get_current_position(motor_idx),
                now);
    }

    /**
     * @breif Computes the size of the longest queue.
     */
    auto longest_queue_size() {
        size_t max_size = 0;
        for(auto& queue : motion_queues_) {
            max_size = std::max(max_size, queue.get_num_enqueued_motions());
        }
        return max_size;
    }

    /**
     * @param now To be returned when all queues are empty.
     * @return Returns the time when all queues will be empty.
     */
    auto get_last_motion_end_time(time_point now) const {
        time_point p;
        for(auto& queue : motion_queues_) {
            p = std::max(p, queue.get_last_motion_end_time(now));
        }
        return p;
    }

    MultiMotorStepper<NUM_MOTORS> motor_stepper_;
    std::array<MotionQueue<Motion>, NUM_MOTORS> motion_queues_;
};
