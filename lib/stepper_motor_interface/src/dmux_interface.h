#pragma once

#include<bitset>
#include<Arduino.h>

/**
 * @brief A helper to execute motor steps on motors behind a dmux interface.
 */
class DmuxInterface {
public:
    /**
     * @brief Set the pin modes for the pins used by the dmux interface.
     */
    DmuxInterface() {
        // TODO: switch the mode of the entire port at once. Then we do not need to store all the pin numbers below.
        pinMode(addr_pin_0_, OUTPUT);
        pinMode(addr_pin_1_, OUTPUT);
        pinMode(addr_pin_2_, OUTPUT);
        pinMode(addr_pin_3_, OUTPUT);

        pinMode(dir_pin_0_, OUTPUT);
        pinMode(step_pin_0_, OUTPUT);
        pinMode(dir_pin_1_, OUTPUT);
        pinMode(step_pin_1_, OUTPUT);
        pinMode(dir_pin_2_, OUTPUT);
        pinMode(step_pin_2_, OUTPUT);
        pinMode(dir_pin_3_, OUTPUT);
        pinMode(step_pin_3_, OUTPUT);

        pinMode(latch_pin_, OUTPUT);

        digitalWriteFast(latch_pin_, HIGH);
    }

    /**
     * @brief Selects a quartet of stepper motors to drive. 
     * 
     * @note Only the lower nibble of the address is considered. So input values from 0 to 15.
     * 
     * @param address The address of the quartet to select.
     */
    void select_quartet(uint8_t address) {
        // Sets the lowest 4 bits of GPIOD_PDOR to the last four bits of the address
        GPIOD_PDOR = (GPIOD_PDOR & B11110000) | (address & B00001111);
        delayNanoseconds(100);
    }

    /**
     * @brief Does a step with all four motors of the currently selected quartet simultaneously.
     * 
     * The step pins are kept high, a step is induced by pulling the step pins low for a while and then setting them high again.
     * 
     * @param dir1 The direction of motor 1.
     * @param step1 Flag indicating whether motor 1 should do a step.
     * @param dir2 The direction of motor 2.
     * @param step2 Flag indicating whether motor 2 should do a step.
     * @param dir3 The direction of motor 3.
     * @param step3 Flag indicating whether motor 3 should do a step.
     * @param dir4 The direction of motor 4.
     * @param step4 Flag indicating whether motor 4 should do a step.
     */
    void do_step_with_all_motors_of_quartet(
    bool dir1,
    bool step1,
    bool dir2 = false,
    bool step2 = false,
    bool dir3 = false,
    bool step3 = false,
    bool dir4 = false,
    bool step4 = false
    ) {
        std::bitset<8> cmd;
        cmd[0] = dir1;
        cmd[1] = !step1;
        cmd[2] = dir2;
        cmd[3] = !step2;
        cmd[4] = dir3;
        cmd[5] = !step3;
        cmd[6] = dir4;
        cmd[7] = !step4;

        // toggle step bits (where needed)
        GPIOC_PDOR = cmd.to_ulong();
        // delayNanoseconds(10);
        if(step1 or step2 or step3 or step4) {
            toggle_latch();
        }
        // delayNanoseconds(250);
    }

private:
    const uint8_t latch_pin_ = 6;

    // const typeof(GPIOD_PDOR) address_port = GPIOD_PDOR;
    const uint8_t addr_pin_0_ = 2;
    const uint8_t addr_pin_1_ = 14;
    const uint8_t addr_pin_2_ = 7;
    const uint8_t addr_pin_3_ = 8;

    // const typeof(GPIOC_PDOR) command_port = GPIOC_PDOR;
    const uint8_t dir_pin_0_ = 15;
    const uint8_t step_pin_0_ = 22;
    const uint8_t dir_pin_1_ = 23;
    const uint8_t step_pin_1_ = 9;
    const uint8_t dir_pin_2_ = 10;
    const uint8_t step_pin_2_ = 13;
    const uint8_t dir_pin_3_ = 11;
    const uint8_t step_pin_3_ = 12;
    

    /**
     * @brief Toggles the latch pin of the DMUX
     * 
     * Will set all data outputs of the DMUX to HIGH.
     */
    void toggle_latch() {
        digitalWriteFast(latch_pin_, LOW);
        delayNanoseconds(100);

        digitalWriteFast(latch_pin_, HIGH);
        delayNanoseconds(100);
    }
};

