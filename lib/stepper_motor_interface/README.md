The `stepper_motor_interface` library lets you move stepper motors attached via the EC stepper muxing interface to a teensy microcontroller.

Starting from lower-level to higher-level operations it contains:

- `dmux_interface.h` to toggle pins in such a way that the right steps arrive at the right motors behing the multiplexer.
- `multi_motor_stepper.h` to memorize motor positions and compute the steps that make motors to a given target position.
- `*_motor_mover.h` to compute motor target positions that change over time.
- `absolute_motion.h` a container to describe motions, which are executed by the motor movers.

It depends on the `motion_math` library to compute smothe acceleration and deceleration curves for the motors.