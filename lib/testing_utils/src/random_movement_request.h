#pragma once

#include<random>
#include <movement_request.h>


/**
 * @brief Generates random movement requests.
 *
 * Destinations are sampled uniformly from [-100, 100].
 * The max velocities are sampled uniformly from [0.2*max_vel, max_max_vel].
 * End velocity is 0 with 0.1 probability, otherwise sampled uniformly from [-max_vel, max_vel].
 * Acceleration is sampled uniformly from [5, 10].
 * Deceleration is sampled uniformly from [5, 10].
 * Duration is 0 with 0.1 probability, otherwise sampled uniformly from [0, 5].
 *
 * @param max_max_vel The maximal maximum velocity.
 * @return A random movement request.
 */
auto random_movement_request(double max_max_vel) {
    static std::default_random_engine e;

    auto max_vel = std::uniform_real_distribution<double>(max_max_vel*0.2, max_max_vel)(e);
    auto end_vel = std::bernoulli_distribution(0.1)(e) ? 0.0 : std::uniform_real_distribution<double>(-max_vel, max_vel)(e);
    auto duration = std::bernoulli_distribution(0.1)(e) ? 0.0 : std::uniform_real_distribution<double>(0, 5)(e);

    return MovementRequest{
            std::uniform_real_distribution<double>(-100, 100)(e),
            end_vel,
            std::uniform_real_distribution<double>(5, 10)(e),
            std::uniform_real_distribution<double>(5, 10)(e),
            max_max_vel,
            duration};
}