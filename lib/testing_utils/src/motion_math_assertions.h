#pragma once

#include"unity.h"

#include"movement_instruction.h"
#include"accel_decel_motion.h"
#include"utils.h"


void assert_computed_instructions_equal_expected_and_are_valid(const char* msg, double sb, double va, double vb, double a1, double a3, double v_max, double t, const movement_instruction& expected_instruction) {
    auto instruction = compute_movement_instruction(sb, va, vb, a1, a3, v_max, t);

    TEST_ASSERT_FLOAT_WITHIN(0.01, expected_instruction.durations.d1, instruction.durations.d1);
    TEST_ASSERT_FLOAT_WITHIN(0.01, expected_instruction.durations.d2, instruction.durations.d2);
    TEST_ASSERT_FLOAT_WITHIN(0.01, expected_instruction.durations.d3, instruction.durations.d3);

    TEST_ASSERT_FLOAT_WITHIN(0.01, expected_instruction.a1, instruction.a1);
    TEST_ASSERT_FLOAT_WITHIN(0.01, expected_instruction.a3, instruction.a3);
    auto m = AccelDecelMotion{va, instruction};

    if ( not isclose(m.duration, t) ) {
        TEST_ASSERT_MESSAGE(t < m.duration, msg);
    }

    TEST_ASSERT_FLOAT_WITHIN_MESSAGE(0.1, sb, m.destination, msg);
    TEST_ASSERT_FLOAT_WITHIN_MESSAGE(1e-7, vb, m.final_velocity, msg);

    if (not isclose(std::abs(m.travel_velocity), v_max)) {
        TEST_ASSERT_MESSAGE(std::abs(m.travel_velocity) < v_max, msg);
    }
}