The `motion_math` library concerns itself with the computation of the acceleration and deceleration times 
that are needed to move a motor to a certain position while transitioning between different velocities.

Its implementation is mostly a port of the `python_impl` so look there to learn how the math has been derived.