from typing import List, Dict
import sympy as sp
import numpy as np
from IPython.core.display import display


def display_solutions(solutions: List[Dict]) -> None:
    """
    Displays all the solutions found using sympy solve with dict=True
    """
    multiple_soultions = len(solutions) > 1
    if multiple_soultions: print(f"Found {len(solutions)} solutions!")
    for idx, sol in enumerate(solutions):
        if multiple_soultions: print(f"Solution {idx + 1}")

        for key in sol.keys():
            display(sp.Eq(key, sol[key]))


def lambdify_solutions(solutions: List[Dict], parameters: List[sp.Symbol]) -> List[Dict]:
    """
    Lambdifies the equations of solutions found using sympy solve with dict=True using the given parameters.
    
    This allows to call the solution equations as functions.
    """
    return [
        {key: sp.lambdify(parameters, equation) for key, equation in sol.items()}
        for sol in solutions
    ]


small_duration_threshold = 1e-7


def feasibility(*durations):
    if np.any(np.isnan(durations)):
        return -np.infty
    else:
        return np.min(np.clip(durations, -np.infty, 0))
    

def is_feasible_duration(*durations):
    return np.all(-small_duration_threshold < np.asarray(durations))


def snap_small_durations_to_zero(*durations):
    return tuple(0 if -small_duration_threshold < d < small_duration_threshold else d for d in durations)