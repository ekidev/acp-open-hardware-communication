from movement_instruction import MovementInstruction


class RelativeMotion:
    def __init__(self, start_velocity: float, instruction: MovementInstruction):
        self.start_velocity = start_velocity
        self.d1 = instruction.d1
        self.d2 = instruction.d2
        self.d3 = instruction.d3
        self.a1 = instruction.a1
        self.a3 = instruction.a3

    @property
    def duration(self):
        return self.d1 + self.d2 + self.d3

    @property
    def travel_velocity(self):
        return self.start_velocity + self.a1 * self.d1

    @property
    def final_velocity(self):
        return self.travel_velocity + self.a3 * self.d3

    @property
    def travel_start(self):
        return self.start_velocity * self.d1 + (self.a1 * self.d1 ** 2)/2

    @property
    def travel_start_time(self):
        return self.d1

    @property
    def travel_end_time(self):
        return self.d1 + self.d2

    @property
    def travel_end(self):
        return self.travel_start + self.travel_velocity * self.d2

    @property
    def destination(self):
        return self.travel_end + self.travel_velocity * self.d3 + (self.a3 * self.d3 ** 2)/2

    def position(self, t):
        if 0 <= t <= self.travel_start_time:
            return self.start_velocity * t + (self.a1 * t ** 2) / 2
        if self.travel_start_time < t <= self.travel_end_time:
            t_ = t - self.travel_start_time
            return self.travel_start + self.travel_velocity * t_
        if self.travel_end_time < t <= self.duration:
            t_ = t - self.travel_end_time
            return self.travel_end + self.travel_velocity * t_ + (self.a3 * t_ ** 2) / 2
        return None

    def velocity(self, t):
        if t < 0:
            return self.start_velocity

        if 0 <= t < self.travel_start_time:
            t_ = t
            return self.start_velocity + self.a1 * t_
        
        if self.travel_start_time <= t < self.travel_end_time:
            return self.travel_velocity
        
        if self.travel_end_time <= t < self.duration:
            t_ = t - self.travel_end_time
            return self.travel_velocity + self.a3 * t_
        
        return self.final_velocity
