class MovementInstruction:
    def __init__(self, d1, d2, d3, a1, a3) -> None:
        self.d1 = d1
        self.d2 = d2
        self.d3 = d3
        self.a1 = a1
        self.a3 = a3

    def __repr__(self):
        return f"Durations: {self.d1}, {self.d2}, {self.d3} Accelerations: {self.a1}, {self.a3}"
