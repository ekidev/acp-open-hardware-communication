import numpy as np
from hypothesis import given, settings, assume, event

from test_utils import positions, velocities, accelerations, velocity_limits, is_sane_velocities, durations
import motion_math_symbolic as symbolic
import motion_math_numeric as numeric


@given(destination=positions,
       start_velocity=velocities,
       end_velocity=velocities,
       acceleration=accelerations,
       deacceleration=accelerations,
       duration=durations)
@settings(max_examples=5000, deadline=None)
def test_numeric_matches_symbolic_total_duration(destination, start_velocity, end_velocity, acceleration, deacceleration, duration):
    symbolic_solutions = symbolic.generate_phase_durations_given_total_duration(destination, start_velocity, end_velocity, acceleration, deacceleration, duration)
    numeric_solutions = numeric.generate_phase_durations_given_total_duration(destination, start_velocity, end_velocity, acceleration, deacceleration, duration)

    for symbolic_solution, numeric_solution in zip(symbolic_solutions, numeric_solutions):
        if symbolic_solution is None or numeric_solution is None:
            assert symbolic_solution is None and numeric_solution is None
        else:
            event("found solution")
            assert np.all(np.isclose(symbolic_solution, numeric_solution, atol=1e-7))


@given(destination=positions,
       start_velocity=velocities,
       end_velocity=velocities,
       acceleration=accelerations,
       deacceleration=accelerations,
       max_velocity=velocity_limits)
@settings(max_examples=5000, deadline=None)
def test_numeric_matches_symbolic_v_max(destination, start_velocity, end_velocity, acceleration, deacceleration, max_velocity):
    assume(is_sane_velocities(start_velocity, end_velocity, max_velocity))
    symbolic_solutions = symbolic.generate_phase_durations_given_v_max(destination, start_velocity, end_velocity, acceleration, deacceleration, max_velocity)
    numeric_solutions = numeric.generate_phase_durations_given_v_max(destination, start_velocity, end_velocity, acceleration, deacceleration, max_velocity)

    for symbolic_solution, numeric_solution in zip(symbolic_solutions, numeric_solutions):
        if symbolic_solution is None or numeric_solution is None:
            assert symbolic_solution is None and numeric_solution is None
        else:
            event("found solution")
            assert np.all(np.isclose(symbolic_solution, numeric_solution, atol=1e-7))


@given(destination=positions,
       start_velocity=velocities,
       end_velocity=velocities,
       acceleration=accelerations,
       deacceleration=accelerations)
@settings(max_examples=5000, deadline=None)
def test_numeric_matches_symbolic_no_travel(destination, start_velocity, end_velocity, acceleration, deacceleration):
    symbolic_solutions = symbolic.generate_phase_durations_given_no_travel( destination, start_velocity, end_velocity, acceleration, deacceleration)
    numeric_solutions = numeric.generate_phase_durations_given_no_travel(destination, start_velocity, end_velocity, acceleration, deacceleration)

    for symbolic_solution, numeric_solution in zip(symbolic_solutions, numeric_solutions):
        if symbolic_solution is None or numeric_solution is None:
            assert symbolic_solution is None and numeric_solution is None
        else:
            event("found solution")
            assert np.all(np.isclose(symbolic_solution, numeric_solution, atol=1e-7))
