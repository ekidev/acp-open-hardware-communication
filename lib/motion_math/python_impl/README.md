# Motion Math

In this folder, the math to move a motor from a start position (and velocity) to an end position (and velocity)
within a given time is derived and implemented in python.
This serves as a prototype to be ported to embedded C++ to run on the Teensy.

The jupyter notebook (`motion_math.ipynb`) the formulas based on the physical equations of motion 
with a little help from the symbolic math library [SymPy](www.sympy.org).

These derived formulas are implemented as python functions twice.
Once in `motion_math_symbolic.py` and once in `motion_math_numeric.py`.
The symbolic evaluation of the formulas is done again via SymPy and offers superior accuracy.
It mostly serves as a gold standard for testing the numerical implementation, 
that uses only standard math functions for evaluation.

The file `movement_instruction_computation.py` contains a function that makes use of the formula implementations 
to actually compute instructions to perform a movement.
That means for how long to accelerate, travel and decelerate to perform a given movement.
Note, that the formulas can not be used right away, since they often yield more than one solution and there are 
various different formulas for different edge cases.

The three test files ensure that the computed motion instructions are valid (`test_motion_instruction.py`),
that the numeric implementations match the symbolic gold standard (`test_numeric_symbolic_match.py`)
and that the results of the derived formulas are actually valid (`test_motion_math_validity.py`).
For this the [Hypothesis](hypothesis.works) testing framework is used on top of [pytest](pytest.org).

## Notation

In this code we often talk about movements, that are split up in three phases:
1. Acceleration
2. Travel
3. Deceleration

As illustrated here:

![asd](images/three_phases.svg)

A movement starts at position 0 with velocity `va` and accelerates at the rate of `a1` for `d` seconds.
After that position `s1` velocity `v1` have been reached at the end of phase 1, the acceleration phase.

Then the movement continues without acceleration for `d2` seconds until position `s2` has been reached.
This is the travel phase with `v1 = travel_velocity = v2`.

Finally, it decelerates for another `d3` seconds at rate `a3` 
until position `s3 = sb` has been reached at velocity `v3 = vb`.

Note, that typically initial and final velocities are zero: `va = vb = 0` but in some cases it might be
desirable to start a movement while already moving (with nonzero `va`) or finish it with some velocity `vb`
to continue with the next movement without stopping.

So in total, the following terms are used throughout the code:

- `va` start velocity of the movement
- `sb` end position of the movement
- `vb` end velocity of the movement
- `a1` acceleration during acceleration phase 1
- (`a2`) acceleration during travel phase 2 (never used since `a2 = 0`)
- `a3` acceleration during deceleration phase 3
- `s1` position after phase 1
- `s2` position after phase 2
- `s3` position after phase 3 (equal to `sb`)
- `v1` velocity after acceleration in phase 1
- `v2` velocity after the travel phase (equal to `v1` since there is no acceleration during travel)
- `v3` velocity after phase 3 (equal to `vb`)