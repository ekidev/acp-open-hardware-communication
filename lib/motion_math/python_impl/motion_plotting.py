import numpy as np
import matplotlib.pyplot as plt

def show_movement(va, a1, a3, d1, d2, d3):
    """
    Displays a one-dimensional movement along the y-axis through time with
    an acceleration phase, a travel phase and a deacceleration phase.

    Note, that if the given parameters make no sense, then the resulting plot will also make no sense 
    (e.g. durations should be positive).

    :param sa: The initial position.
    :param va: The initial velocity.
    :param a1: The acceleration during the first acceleration phase.
    :param a3: The (de)acceleration during the breaking phase.
    :param d1: The duration of the acceleration phase.
    :param d2: The duration of the travel phase.
    :param d3: The duration of the deacceleration phase.
    """
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)

    
    s1 = va*d1 + 0.5*a1*d1**2
    v1 = va + a1*d1

    s2 = s1 + v1*d2

    t1 = np.linspace(0, d1, 100)
    t2 = np.linspace(d1, d1+d2, 100)
    t3 = np.linspace(d1+d2, d1+d2+d3, 100)
    
    ax.plot(t1, va * t1 + 0.5 * a1 * (t1 ** 2), label='Acceleration')
    ax.plot(t2, s1 + v1 * (t2 - d1), label='Travel')
    ax.plot(t3, s2 + v1*(t3 - (d1+d2)) + 0.5 * a3 * (t3 - (d1+d2))**2, label='Deacceleration')
    
    ax.set_xlabel("Time")
    ax.set_ylabel("Position")
    ax.set_xlim(0, 200)
    ax.set_ylim(-110, 110)
    fig.canvas.draw_idle()
    return fig