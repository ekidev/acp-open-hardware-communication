import numpy as np


def generate_phase_durations_given_v_max(sb, va, vb, a1, a3, v_max):
    if a1 == 0 or a1 * a3 * v_max == 0 or a3 == 0:
        return

    d1 = -(va - v_max) / a1
    d2 = (2 * a1 * a3 * sb - a1 * vb ** 2 + a1 * v_max**2 + a3 * va ** 2 - a3*v_max ** 2) / (2 * a1 * a3 * v_max)
    d3 = (vb - v_max) / a3

    if not np.any(np.isnan((d1, d2, d3))):
        yield d1, d2, d3


def generate_phase_durations_given_total_duration(sb, va, vb, a1, a3, t):
    if not np.isclose(a1, a3):
        accel_difference = a1 - a3
        if a1 * accel_difference == 0 or a3 * accel_difference == 0 or a1 * a3 == 0:
            return

        sqrt_arg = a1 * a3 * (a1 * a3 * t ** 2 + 2 * a1 * sb - 2 * a1 * t * vb - 2 * a3 * sb + 2 * a3 * t * va + va ** 2 - 2 * va * vb + vb ** 2)
        if sqrt_arg < 0:
            return
        sqrt_term = np.sqrt(sqrt_arg)

        # First solution
        d1 = (-a1 * a3 * t - a1 * va + a1 * vb - sqrt_term) / (a1 * accel_difference)
        d2 = -sqrt_term/(a1 * a3)
        d3 = (a1 * t + va - vb) / accel_difference + sqrt_term / (a3 * accel_difference)

        if not np.any(np.isnan((d1, d2, d3))):
            yield d1, d2, d3

        # Second solution
        d1 = (-a1 * a3 * t - a1 * va + a1 * vb + sqrt_term) / (a1 * accel_difference)
        d2 = sqrt_term/(a1 * a3)
        d3 = (a1 * t + va - vb) / accel_difference - sqrt_term / (a3 * accel_difference)
        if not np.any(np.isnan((d1, d2, d3))):
            yield d1, d2, d3
    else:
        a = (a1 + a3) / 2

        if (a*(a * t + va - vb)) == 0:
            return

        d1 = (a * sb - a * t * va - va ** 2 / 2 + va * vb - vb ** 2 / 2) / (a * (a * t + va - vb))
        d2 = (a * t + va - vb) / a
        d3 = (- a * sb + a * t * vb - va ** 2 / 2 + va * vb - vb ** 2 / 2) / (a * (a * t + va - vb))

        if not np.any(np.isnan((d1, d2, d3))):
            yield d1, d2, d3

    return None


def generate_phase_durations_given_no_travel(sb, va, vb, a1, a3):
    accel_difference = a1 - a3
    if accel_difference == 0 or a1 == 0 or a3 == 0:
        return
    sqrt_arg = accel_difference*( - 2 * a1 * a3 * sb + a1 * vb ** 2 - a3 * va ** 2)
    if sqrt_arg < 0:
        return
    sqrt_term = np.sqrt(sqrt_arg)

    # Solution 1
    d3 = vb / a3 - sqrt_term / (a3 * accel_difference)
    d1 = -(a3 * d3 + va - vb) / a1
    if not np.isnan(d1) and not np.isnan(d3):
        yield d1, 0, d3

    # Solution 2
    d3 = vb / a3 + sqrt_term / (a3 * accel_difference)
    d1 = -(a3 * d3 + va - vb) / a1
    if not np.any(np.isnan((d1, d3))):
        yield d1, 0, d3
