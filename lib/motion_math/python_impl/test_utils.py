import hypothesis.strategies as st

movement_range = 6400 * 20
velocity_range = 100000
acceleration_range = 10000

positions = st.integers(min_value=-movement_range, max_value=movement_range)
velocities = st.floats(allow_infinity=False, allow_nan=False, min_value=-velocity_range, max_value=velocity_range)
accelerations = st.floats(allow_infinity=False, allow_nan=False, min_value=-acceleration_range, max_value=acceleration_range)
acceleration_limits = st.floats(allow_infinity=False, allow_nan=False, min_value=0.1, max_value=acceleration_range)
velocity_limits = st.floats(allow_infinity=False, allow_nan=False, min_value=0.1, max_value=velocity_range)
durations = st.floats(allow_infinity=False, allow_nan=False, min_value=0.1, max_value=500)


def is_sane_velocities(start_velocity, end_velocity, max_velocity):
    return abs(start_velocity) <= abs(max_velocity) and \
           abs(end_velocity) <= abs(max_velocity)
