import numpy as np

from movement_instruction import MovementInstruction
from motion_math_numeric import generate_phase_durations_given_v_max, generate_phase_durations_given_total_duration, \
    generate_phase_durations_given_no_travel
from utils import is_feasible_duration, feasibility, snap_small_durations_to_zero


def generate_movement_instructions_given_total_duration(sb, va, vb, a1, a3, t):
    for a1_ in (a1, -a1):
        for a3_ in (a3, -a3):
            for d1, d2, d3 in generate_phase_durations_given_total_duration(sb, va, vb, a1_, a3_, t):
                yield MovementInstruction(d1, d2, d3, a1_, a3_)
    return None


def generate_movement_instructions_given_v_max(sb, va, vb, a1, a3, v_max):
    for a1_ in (a1, -a1):
        for a3_ in (a3, -a3):
            for v_max in (v_max, -v_max):
                for d1, d2, d3 in generate_phase_durations_given_v_max(sb, va, vb, a1_, a3_, v_max):
                    yield MovementInstruction(d1, d2, d3, a1_, a3_)


def generate_movement_instructions_given_no_travel(sb, va, vb, a1, a3):
    for a1_ in (a1, -a1):
        for a3_ in (a3, -a3):
            for d1, d2, d3 in generate_phase_durations_given_no_travel(sb, va, vb, a1_, a3_):
                yield MovementInstruction(d1, d2, d3, a1_, a3_)


def compute_movement_instruction(destination, start_velocity, end_velocity, max_acceleration, max_deacceleration, max_velocity, duration=None) -> MovementInstruction:
    if duration is not None:

        def feasibility_given_desired_duration(i: MovementInstruction):
            f = feasibility(i.d1, i.d2, i.d3)
            if f == 0:
                effective_duration = sum(snap_small_durations_to_zero(i.d1, i.d2, i.d3))
                duration_error = abs(effective_duration - duration)
                return duration - duration_error
            else:
                return f

        instruction = max(generate_movement_instructions_given_total_duration(destination, start_velocity, end_velocity, max_acceleration, max_deacceleration, duration),
                           key=feasibility_given_desired_duration)

        if is_feasible_duration(instruction.d1, instruction.d2, instruction.d3):
            instruction.d1, instruction.d2, instruction.d3 = snap_small_durations_to_zero(instruction.d1, instruction.d2, instruction.d3)
            travel_velocity = start_velocity + instruction.a1 * instruction.d1
            if abs(travel_velocity) <= abs(max_velocity):
                return instruction

    def feasibility_given_vmax(i: MovementInstruction):
        f = feasibility(i.d1, i.d2, i.d3)
        if f != -np.infty:
            d1, d2, d3 = snap_small_durations_to_zero(i.d1, i.d2, i.d3)
            d = np.sum((d1, d2, d3))
            if d < duration and not np.isclose(d, duration):
                return -np.infty

            travel_velocity = start_velocity + i.a1 * d1
            if abs(travel_velocity) > abs(max_velocity) and not np.isclose(abs(travel_velocity), abs(max_velocity)):
                return -np.infty
        return f

    instruction = max(generate_movement_instructions_given_v_max(destination, start_velocity, end_velocity,
                                                                 max_acceleration, max_deacceleration, max_velocity),
                      key=feasibility_given_vmax)

    instructions_no_travel = max(generate_movement_instructions_given_no_travel(destination, start_velocity,
                                                                          end_velocity, max_acceleration, max_deacceleration),
                                 key=feasibility_given_vmax)

    instruction = max(instructions_no_travel, instruction, key=feasibility_given_vmax)

    instruction.d1, instruction.d2, instruction.d3 = snap_small_durations_to_zero(instruction.d1,
                                                                                  instruction.d2,
                                                                                  instruction.d3)
    return instruction
