import numpy as np
from hypothesis import assume, settings, given, event

from motion_math_numeric import generate_phase_durations_given_v_max, generate_phase_durations_given_no_travel, \
    generate_phase_durations_given_total_duration
from relative_motion import RelativeMotion
from movement_instruction import MovementInstruction
from test_utils import positions, velocities, accelerations, velocity_limits, is_sane_velocities, durations
from utils import small_duration_threshold, feasibility, snap_small_durations_to_zero


@given(destination=positions,
       start_velocity=velocities,
       end_velocity=velocities,
       acceleration=accelerations,
       deacceleration=accelerations,
       duration=durations)
@settings(max_examples=10000)
def test_durations_given_total_duration_are_valid(destination, start_velocity, end_velocity, acceleration, deacceleration, duration):
    solutions = generate_phase_durations_given_total_duration(destination, start_velocity, end_velocity, acceleration, deacceleration, duration)

    for solution in solutions:
        if feasibility(*solution) > -small_duration_threshold:
            event("found solution")
            d1, d2, d3 = snap_small_durations_to_zero(*solution)
            instruction = MovementInstruction(d1, d2, d3, acceleration, deacceleration)
            movement = RelativeMotion(start_velocity, instruction)

            assert d1 >= 0
            assert d2 >= 0
            assert d3 >= 0
            assert np.isclose(movement.duration, duration)
            assert np.isclose(end_velocity, movement.final_velocity)
            assert np.isclose(destination, movement.destination, atol=0.01)


@given(destination=positions,
       start_velocity=velocities,
       end_velocity=velocities,
       acceleration=accelerations,
       deacceleration=accelerations,
       max_velocity=velocity_limits)
@settings(max_examples=5000, deadline=None)
def test_durations_given_v_max_are_valid(destination, start_velocity, end_velocity, acceleration, deacceleration, max_velocity):
    assume(is_sane_velocities(start_velocity, end_velocity, max_velocity))
    solutions = generate_phase_durations_given_v_max(destination, start_velocity, end_velocity, acceleration, deacceleration, max_velocity)

    for solution in solutions:
        if feasibility(*solution) > -small_duration_threshold:
            event("found solution")
            d1, d2, d3 = snap_small_durations_to_zero(*solution)
            instruction = MovementInstruction(d1, d2, d3, acceleration, deacceleration)
            movement = RelativeMotion(start_velocity, instruction)

            assert d1 >= 0
            assert d2 >= 0
            assert d3 >= 0
            assert np.isclose(max_velocity, movement.travel_velocity)
            assert np.isclose(end_velocity, movement.final_velocity)
            assert np.isclose(destination, movement.destination)


@given(destination=positions,
       start_velocity=velocities,
       end_velocity=velocities,
       acceleration=accelerations,
       deacceleration=accelerations)
@settings(max_examples=5000, deadline=None)
def test_durations_given_no_travel_are_valid(destination, start_velocity, end_velocity, acceleration, deacceleration):
    solutions = generate_phase_durations_given_no_travel(destination, start_velocity, end_velocity, acceleration, deacceleration)

    for solution in solutions:
        if feasibility(*solution) > -small_duration_threshold:
            event("found solution")
            d1, d2, d3 = snap_small_durations_to_zero(*solution)
            instruction = MovementInstruction(d1, d2, d3, acceleration, deacceleration)
            movement = RelativeMotion(start_velocity, instruction)

            assert d1 >= 0
            assert d2 >= 0
            assert d3 >= 0
            assert np.isclose(end_velocity, movement.final_velocity)
            assert np.isclose(destination, movement.destination)

