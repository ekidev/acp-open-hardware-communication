import numpy as np
import sympy as sp

sb, va, vb = sp.symbols("s_B, v_A, v_B", real=True)
a1, a3 = sp.symbols("a_1, a_3", real=True)

d1, d2, d3 = sp.symbols("d_1, d_2, d_3", real=True)
t = sp.symbols("t", real=True)


s1 = va*d1 + sp.Rational(1/2)*a1*d1**2  # the position after phase 1
v1 = va + a1*d1  # the velocity after phase 1

s2 = s1 + v1*d2  # the position after phase 2
v2 = v1  # the velocity after phase 2 (the same since there was no acceleration)

s3 = s2 + v1*d3 + sp.Rational(1/2) * a3 * d3**2  # the final position after phase 3
v3 = v2 + a3*d3  # the final velocity after phase 3

v_max = sp.Symbol("v_max", real=True, positive=True)

durations_given_v_max_equations = sp.solve(
    [sp.Eq(s3, sb), sp.Eq(v3, vb), sp.Eq(v1, v_max)],
    [d1, d2, d3],
    dict=True)


def generate_phase_durations_given_v_max(sb_, va_, vb_, a1_, a3_, vmax_):

    if a1_ == 0 or a1_ * a3_ * vmax_ == 0 or a3_ == 0:
        return

    d1_, d2_, d3_ = (durations_given_v_max_equations[0][d].subs(
        [(sb, sb_), (va, va_), (vb, vb_), (a1, a1_), (a3, a3_), (v_max, vmax_)]
    ) for d in (d1, d2, d3))

    if d1_.is_real and d2_.is_real and d3_.is_real:
        yield float(d1_), float(d2_), float(d3_)


phase_durations_given_total_duration_equations = sp.solve(
    [sp.Eq(s3, sb), sp.Eq(v3, vb), sp.Eq(t, d1 + d2 + d3)],
    [d1, d2, d3],
    dict=True)

a = sp.symbols("a", real=True)

phase_durations_given_total_duration_equations.extend(sp.solve(
    [sp.Eq(s3.subs([(a1, a), (a3, a)]), sb), sp.Eq(v3.subs([(a1, a), (a3, a)]), vb), sp.Eq(t, d1 + d2 + d3)],
    [d1, d2, d3],
    dict=True))


def generate_phase_durations_given_total_duration(sb_, va_, vb_, a1_, a3_, t_):

    if not np.isclose(a1_, a3_):

        for equations in phase_durations_given_total_duration_equations:

            d1_, d2_, d3_ = (equations[d].subs(
                [(sb, sb_), (va, va_), (vb, vb_), (a1, a1_), (a3, a3_), (t, t_)]
            ) for d in (d1, d2, d3))
            if d1_.is_real and d2_.is_real and d3_.is_real:
                yield float(d1_), float(d2_), float(d3_)

    else:
        d1_, d2_, d3_ = (phase_durations_given_total_duration_equations[2][d].subs(
            [(sb, sb_), (va, va_), (vb, vb_), (a, a1_), (a, a3_), (t, t_)]
        ) for d in (d1, d2, d3))
        if d1_.is_real and d2_.is_real and d3_.is_real:
            yield float(d1_), float(d2_), float(d3_)


s_middle = va * d1 + sp.Rational(1, 2) * a1 * d1 ** 2
v_middle = va + a1 * d1

s_end = s_middle + v_middle * d3 + sp.Rational(1, 2) * a3 * d3 ** 2
v_end = v_middle + a3 * d3

durations_given_no_travel_equations = sp.solve(
    [sp.Eq(s_end, sb), sp.Eq(v_end, vb)],
    [d1, d3],
    dict=True
)


def generate_phase_durations_given_no_travel(sb_, va_, vb_, a1_, a3_):

    if (a1_ - a3_) == 0 or a1_ == 0 or a3_ == 0:
        return

    for equations in durations_given_no_travel_equations:
        d1_, d3_ = (equations[d].subs([(sb, sb_), (va, va_), (vb, vb_), (a1, a1_), (a3, a3_)])
                    for d in (d1, d3))
        if d1_.is_real and d3_.is_real:
            yield float(d1_), 0, float(d3_)
