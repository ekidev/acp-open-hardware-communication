/**
 * @file
 */
#pragma once

#include<cmath>
#include<limits>
#include<array>

#include<utils.h>

static constexpr double small_duration_threshold = 1e-7;

/**
 * @brief Describes the durations of the three phases of an `AccelDecelMotion`, which conists of 
 * acceleration (d1), travel at constant speed (d2) and deacceleration (d3).
 * 
 * @note Default constructed phase durations a not feasible.
 */
struct phase_durations {
    double d1 = std::numeric_limits<double>::signaling_NaN();
    double d2 = std::numeric_limits<double>::signaling_NaN();
    double d3 = std::numeric_limits<double>::signaling_NaN();  

    bool is_nan() const {
        return isnanf(d1) or isnanf(d2) or isnanf(d3);
    }

    /**
     * @brief Compute a measure of feasibility of the phase durations.
     * 
     * Lower values are less feasible. The highest feasibility is 0.
     * 
     * If any of the duration is NaN then the feasibility is -inf since that is least feasible.
     * If all durations are positive, the durations are feasible and the feasibility is 0.
     * Negative durations are usually infeasible but when they are very close to zero, 
     * they could be snapped to zero. 
     * In those cases smaller negative durations are worse than larger negative durations.
     * Therefore the feasibilty is determined by the smallest negative duration.
     * 
     * @return auto feasibility in the range [-inf, 0]
     */
    auto feasibility() const {
        if(is_nan()) {
            return -std::numeric_limits<double>::infinity();
        } else {
            return std::min({
                d1 < 0 ? d1 : 0,
                d2 < 0 ? d2 : 0,
                d3 < 0 ? d3 : 0
                });
        }
    }

    /**
     * @brief Checks if all durations are sufficiently close to or above zero.
     */
    bool is_acceptable() const {
        return d1 > -small_duration_threshold and d2 > -small_duration_threshold and d3 > -small_duration_threshold;
    }

    /**
     * @brief Creates a copy where durations very close to zero are set to zero.
     */
    phase_durations snapped_to_zero() const {
        return {
            snap_to_zero(d1, small_duration_threshold),
            snap_to_zero(d2, small_duration_threshold),
            snap_to_zero(d3, small_duration_threshold)
        };
    }

    /**
     * @brief Computes the sum of all three durations.
     */
    auto sum() const {
        return d1 + d2 + d3;
    }
};

/**
 * @brief Compute phase durations for a desired AccelDecelMotion that reaches the given max velocity.
 * 
 * @note The returned phase durations can be infeasible if the desired motion is not feasible.
 * 
 * @param sb The destination.
 * @param va The initial velocity.
 * @param vb The final velocity.
 * @param a1 The acceleration speed.
 * @param a3 The deceleration speed.
 * @param v_max The travel velocity to reach.
 * @return phase_durations That result in reaching the destination with the given final velocity when starting from the given inital velocity.
 */
inline phase_durations generate_phase_durations_given_v_max(int sb, double va, double vb, double a1, double a3, double v_max) {
    if (a1 == 0 or a1 * a3 * v_max == 0 or a3 == 0)
        return {};

    auto d1 = -(va - v_max) / a1;
    auto d2 = -(- 2 * a1 * a3 * sb + a1 * vb * vb - a1 * v_max * v_max - a3 * va * va + a3 * v_max * v_max) / (2 * a1 * a3 * v_max);
    auto d3 = (vb - v_max) / a3;

    return {d1, d2, d3};
}
    
/**
 * @brief Compute sets of phase durations for a AccelDecelMotion that reaches the destinaten in a given time.
 * 
 * @note There are more than one possible analytical solutions to this problem. 
 *  All will be returned and you have to check yourself if they are feasible.
 * 
 * @param sb The destination.
 * @param va The initial velocity.
 * @param vb The final velocity.
 * @param a1 The acceleration speed.
 * @param a3 The deceleration speed.
 * @param t How long the motion should take.
 * @return std::array<phase_durations, 2> Possible phase durations which might result in the desired motion.
 */
inline std::array<phase_durations, 2> generate_phase_durations_given_total_duration(double sb, double va, double vb, double a1, double a3, double t) {

    if (not isclose(a1, a3)) {
        auto accel_difference = a1 - a3;
        if(a1 * accel_difference == 0 or a3 * accel_difference == 0 or a1 * a3 == 0) return {};

        auto sqrt_arg = a1 * a3 * (a1 * a3 * t * t + 2 * a1 * sb - 2 * a1 * t * vb  - 2 * a3 * sb + 2 * a3 * t * va + va * va - 2 * va * vb + vb * vb);
        if(sqrt_arg < 0) return {};

        auto sqrt_term = std::sqrt(sqrt_arg);

        return {
            phase_durations {
                (-a1 * a3 * t - a1 * va + a1 * vb - sqrt_term) / (a1 * accel_difference),
                -sqrt_term/(a1 * a3),
                (a1 * t + va - vb) / accel_difference + sqrt_term / (a3 * accel_difference)
            },
            phase_durations {
                (-a1 * a3 * t - a1 * va + a1 * vb + sqrt_term) / (a1 * accel_difference),
                sqrt_term/(a1 * a3),
                (a1 * t + va - vb) / accel_difference - sqrt_term / (a3 * accel_difference)
            }
        };
    } else {
        auto a = (a1 + a3) / 2;
        auto divisor = a*(a * t + va - vb);
        if (divisor == 0) return {};

        return {
            phase_durations {
                (a * sb - a * t * va - va * va / 2 + va * vb - vb * vb / 2) / divisor,
                (a * t + va - vb) / a,
                (- a * sb + a * t * vb - va * va / 2 + va * vb - vb * vb / 2) / divisor
            }
        };
    }
}

/**
 * @brief Compute sets of phase durations for a AccelDecelMotion that consists of just acceleration and deceleration phase and no travel phase.
 * 
 * This means that d2 is always 0.
 * 
 * @note There are more than one possible analytical solutions to this problem. 
 *  All will be returned and you have to check yourself if they are feasible.
 * 
 * @param sb The destination.
 * @param va The initial velocity.
 * @param vb The final velocity.
 * @param a1 The acceleration speed.
 * @param a3 The deceleration speed.
 * @return std::array<phase_durations, 2> Possible phase durations which might result in the desired motion.
 */
inline std::array<phase_durations, 2> generate_phase_durations_given_no_travel(double sb, double va, double vb, double a1, double a3) {
    auto accel_difference = a1 - a3;
    if(accel_difference == 0 or a1 == 0 or a3 == 0)
        return {};

    auto sqrt_arg = accel_difference*(- 2 * a1 * a3 * sb + a1 * vb * vb - a3 * va * va);
    if(sqrt_arg < 0)
        return {};
    auto sqrt_term = std::sqrt(sqrt_arg);

    auto d3_1 = vb / a3 - sqrt_term / (a3 * accel_difference);
    auto d3_2 = vb / a3 + sqrt_term / (a3 * accel_difference);
    return {
        phase_durations {
            -(a3 * d3_1 + va - vb) / a1,
            0,
            d3_1
        }, 
        phase_durations {
            -(a3 * d3_2 + va - vb) / a1,
            0,
            d3_2
        }
    };
}