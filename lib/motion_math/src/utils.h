
#pragma once

#include<cmath>

/**
 * @brief Snaps small values to zero.
 * 
 * @tparam T The type of the value to snap to zero.
 * @param v The value to snap.
 * @param epsilon If the values is closer to 0 than epsilon, it is snapped to zero.
 * @return T 0 if |v| < epsilon, else v
 */
template<class T>
inline T snap_to_zero(const T& v, const T& epsilon) {
    return -epsilon < v and v < epsilon ? 0 : v;
}

/**
 * @brief Chacks if two values are close to eachother. Modeled after numpy.isclose().
 * 
 * The the `numpy.isclose` documentation for more details.
 * https://numpy.org/doc/1.22/reference/generated/numpy.isclose.html?highlight=isclose#numpy.isclose
 * 
 * @tparam T The type of values to compare.
 * @param v1 The first value to compare.
 * @param v2 The second value to compare.
 * @param atol The absolute tolerance below which we would consider the values to be equal.
 * @param rtol The relative tolerance below which we would the values to be equal.
 * @return true If the values are close, false otherwise.
 */
template<class T>
inline bool isclose(const T& v1, const T& v2,  T atol = 1e-08, T rtol = 1e-05) {
    return std::abs(v1 - v2) <= (atol + rtol * std::abs(v2));
}