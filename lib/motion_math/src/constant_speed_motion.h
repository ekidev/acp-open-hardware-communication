#pragma once
#include<cmath>

/**
 * @brief Describes a relative motion that involves just traveling at at constant speed.
 */
struct ConstantSpeedMotion {
    int  target_ = 0;
    double duration_ = 0;

    /**
     * @name Derived attributes
     * These attributes derived from the above public attributes.
     * They are public themselves for simplicity and ease of access, though they should not be modified.
     */
    ///@{
    double velocity_ = std::isnan(target_/duration_) ? 0 : target_/duration_;
    ///@}

    /**
     * @brief Computes the (relative) position of the motion at a given point in time (relative to the start time of the motion).
     */
    double operator () (double t) const {
        if (t < 0) return 0.0;
        if (duration_ < t) return target_;
        return t * velocity_;
    }

    /**
     * @brief Compute the velocity of the motion at a given point in time (relative to the start time of the motion).
     */
    double get_velocity(double t) const {
        if (t < 0) return 0.0;
        if (t > duration_) return 0.0;
        return velocity_;
    }
};