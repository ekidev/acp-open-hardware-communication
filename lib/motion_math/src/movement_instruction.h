/**
 * @file
 */
#pragma once

#include<array>
#include<algorithm>

#include<phase_duration.h>

/**
 * @brief Describes instructions to move a motor split up in three phases.
 * 
 * phase | action                      | duration
 * ------|-----------------------------|-------------
 * 1     | accelerate with rate a1     | durations.d1
 * 2     | travel without acceleration | durations.d2
 * 3     | deceleratwe with rate a3    | durations.d3
 * 
 * Note that usually a1 and a3 would have opposite signs. 
 * Otherwise you have two successive acceleration or deceleration phases.
 */
struct movement_instruction {
    /**
     * @brief Describes the lenght of the movement phases.
     */
    phase_durations durations;

    /**
     * @brief Describes the acceleration amounts during phase 1.
     */
    double a1 = 0;

    /**
     * @brief Describes the acceleration amounts during phase 3.
     */
    double a3 = 0;
};

/**
 * @brief Computes sets of movement instructions to perform a desired motion within a given timeframe.
 * 
 * For this all combinations of a1 and -a1 as well as a3 and -a3 are tried since it is unclear what sequence
 * of operation best reaches the destination at the desired velocity.
 * 
 * @note There are more than one possible analytical solutions to this problem. 
 *  All will be returned and you have to check yourself if they are feasible.
 * 
 * @param sb The destination.
 * @param va The initial velocity.
 * @param vb The final velocity.
 * @param a1 The acceleration speed.
 * @param a3 The deceleration speed.
 * @param t How long the motion should take.
 * @return std::array<movement_instruction, 8> Possible movement instructions which might result in the desired motion.
 */
inline std::array<movement_instruction, 8> generate_movement_instructions_given_total_duration(double sb, double va, double vb, double a1, double a3, double t) {   
    auto ret = std::array<movement_instruction, 8>{};
    size_t idx = 0;
    for(auto a1_ : {a1, -a1}) {
        for(auto a3_ : {a3, -a3}) {
            for (auto durations : generate_phase_durations_given_total_duration(sb, va, vb, a1_, a3_, t)) {
                ret[idx] = movement_instruction{durations, a1_, a3_};
                idx++;
            }
        }
    }
    return ret;
}


/**
 * @brief Compute movement instructions for a desired AccelDecelMotion that reaches the given max velocity.
 * 
 * For this all combinations of (a1, -a1), (a3, -a3) and (v_max, -v_max) are tried since it is unclear what sequence
 * of operation best reaches the destination at the desired velocity.
 * 
 * @note There are more than one possible analytical solutions to this problem. 
 *  All will be returned and you have to check yourself if they are feasible.
 * 
 * @param sb The destination.
 * @param va The initial velocity.
 * @param vb The final velocity.
 * @param a1 The acceleration speed.
 * @param a3 The deceleration speed.
 * @param v_max The travel velocity to reach.
 * @return std::array<movement_instruction, 8> Possible motion instructions, which might result in the desired motion.
 */
inline std::array<movement_instruction, 8> generate_movement_instructions_given_v_max(double sb, double va, double vb, double a1, double a3, double v_max) {
    auto ret = std::array<movement_instruction, 8>{};
    size_t idx = 0;
    for (auto a1_ : {a1, -a1}) {
        for (auto a3_ : {a3, -a3}) {
            for (auto v_max_ : {v_max, -v_max}) {
                ret[idx] = movement_instruction{generate_phase_durations_given_v_max(sb, va, vb, a1_, a3_, v_max_), a1_, a3_};
                idx++;
            }
        }
    }
    return ret;
}

/**
 * @brief Compute sets of movement instructions for a AccelDecelMotion that consists of just acceleration and deceleration phase and no travel phase.
 * 
 * For this all combinations of a1 and -a1 as well as a3 and -a3 are tried since it is unclear what sequence
 * of operation best reaches the destination at the desired velocity.
 * 
 * @note d2 is always 0.
 * 
 * @note There are more than one possible analytical solutions to this problem. 
 *  All will be returned and you have to check yourself if they are feasible.
 * 
 * @param sb The destination.
 * @param va The initial velocity.
 * @param vb The final velocity.
 * @param a1 The acceleration speed.
 * @param a3 The deceleration speed.
 * @return std::array<movement_instruction, 8> Possible movement instructions which might result in the desired motion.
 */
inline std::array<movement_instruction, 8> generate_movement_instructions_given_no_travel(double sb, double va, double vb, double a1, double a3) {
    auto ret = std::array<movement_instruction, 8>{};
    size_t idx = 0;
    for(auto a1_ : {a1, -a1}) {
        for(auto a3_ : {a3, -a3}) {
            for (auto durations : generate_phase_durations_given_no_travel(sb, va, vb, a1_, a3_)) {
                ret[idx] = movement_instruction{durations, a1_, a3_};
                idx++;
            }
        }
    }
    return ret;
}

double extended_feasibility(const movement_instruction& instruction, double duration, double start_velocity, double max_velocity) {
    auto normalized_durations = instruction.durations.snapped_to_zero();

    auto d = normalized_durations.d1 + normalized_durations.d2 + normalized_durations.d3;
    
    if(d < duration and not isclose(d, duration))
        return -std::numeric_limits<double>::infinity();

    auto travel_velocity = start_velocity + instruction.a1 * normalized_durations.d1;

    if(std::abs(travel_velocity) > std::abs(max_velocity) and not isclose(std::abs(travel_velocity), std::abs(max_velocity)))
        return -std::numeric_limits<double>::infinity();

    return instruction.durations.feasibility();
}

double feasible_and_desired_duration(const movement_instruction& i, double desired_duration) {
    auto f = i.durations.feasibility();
        
    if(f == 0) {
        auto duration_error = std::abs(i.durations.snapped_to_zero().sum() - desired_duration);
        return desired_duration - duration_error;
    } else {
        return f;
    }
}

/**
 * @brief Computes a movement instruction to complete the desired motion in the desired time as good as possible.
 * 
 * If the motion can not be performed in the desired duration, the duration is extended such that it becomes feasible.
 * 
 * @note Contrary to other functions in this library, this function ensures that only feasible movement instructions are returned.
 * 
 * @param destination The destination to move to.
 * @param start_velocity The initial velocity to assume.
 * @param end_velocity The final velocity.
 * @param max_acceleration The acceleration speed.
 * @param max_deacceleration The deceleration speed.
 * @param max_velocity The maximum velocity that must not be exceeded
 * @param duration The desired duration of the motion.
 * @return movement_instruction A movement instruction that conforms to the given desired motion as good as possible.
 */
inline movement_instruction compute_movement_instruction(double destination, double start_velocity, double end_velocity, double max_acceleration, double max_deacceleration, double max_velocity, double duration=0) {
    if(duration != 0) {
        auto instructions = generate_movement_instructions_given_total_duration(destination, start_velocity, end_velocity, max_acceleration, max_deacceleration, duration);
        auto& best_instruction = *std::max_element(instructions.begin(), instructions.end(), [&](const movement_instruction& lhs, const movement_instruction& rhs) { return feasible_and_desired_duration(lhs, duration) < feasible_and_desired_duration(rhs, duration); });

        if(best_instruction.durations.is_acceptable()) {
            best_instruction.durations = best_instruction.durations.snapped_to_zero();
            auto travel_velocity = start_velocity + best_instruction.a1 * best_instruction.durations.d1;
            if(std::abs(travel_velocity) <= std::abs(max_velocity)) 
                return best_instruction;
        }
    }


    auto instructions_vmax = generate_movement_instructions_given_v_max(destination, start_velocity, end_velocity, max_acceleration, max_deacceleration, max_velocity);
    auto& instruction_vmax = *std::max_element(instructions_vmax.begin(), instructions_vmax.end(), 
        [&](const movement_instruction& lhs, const movement_instruction& rhs) { return extended_feasibility(lhs, duration, start_velocity, max_velocity) < extended_feasibility(rhs, duration, start_velocity, max_velocity); });


    auto instructions_no_travel = generate_movement_instructions_given_no_travel(destination, start_velocity, end_velocity, max_acceleration, max_deacceleration);
    auto& instruction_no_travel = *std::max_element(instructions_no_travel.begin(), instructions_no_travel.end(),
        [&](const movement_instruction& lhs, const movement_instruction& rhs) { return extended_feasibility(lhs, duration, start_velocity, max_velocity) < extended_feasibility(rhs, duration, start_velocity, max_velocity); });

    auto instruction = std::max(instruction_no_travel, instruction_vmax, [&](const movement_instruction& lhs, const movement_instruction& rhs) { return extended_feasibility(lhs, duration, start_velocity, max_velocity) < extended_feasibility(rhs, duration, start_velocity, max_velocity); });

    instruction.durations = instruction.durations.snapped_to_zero();

    return instruction;
}
