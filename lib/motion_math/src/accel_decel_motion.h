#pragma once

#include<limits>
#include<movement_instruction.h>

/**
 * @brief Describes a relative motion consisting of acceleration, constant speed travel and deceleration.
 */
struct AccelDecelMotion {
    double start_velocity = 0;
    movement_instruction instruction;
    
    /**
     * @name Derived attributes
     * These attributes derived from the above public attributes.
     * They are public themselves for simplicity and ease of access, though they should not be modified.
     */
    ///@{
    double duration = instruction.durations.sum();
    double travel_velocity = start_velocity + instruction.a1 * instruction.durations.d1;
    double final_velocity = travel_velocity + instruction.a3 * instruction.durations.d3;
    double travel_start = start_velocity * instruction.durations.d1 + (instruction.a1 * instruction.durations.d1 * instruction.durations.d1)/2;
    double travel_start_time = instruction.durations.d1;
    double travel_end_time = instruction.durations.d1 + instruction.durations.d2;
    double travel_end = travel_start + travel_velocity * instruction.durations.d2;
    double destination = travel_end + travel_velocity * instruction.durations.d3 + (instruction.a3 * instruction.durations.d3 * instruction.durations.d3)/2;
    ///@}

    // TODO: the way in which position and velocity calculation is implemented here is wrong:
    //      for times past the duration, the position stays fixed while the velocity stays at the final velocity
    //      this makes no sense!

    /**
     * @brief Computes the (relative) position of the motion at a given point in time (relative to the start time of the motion).
     */
    double operator() (double t) const {
        if(t < 0) return 0;
        if(0 <= t and t < travel_start_time) {
            auto t_ = t;
            return start_velocity * t_ + (instruction.a1 * t_ * t_) / 2;
        }
        if(travel_start_time <= t and t < travel_end_time) {
            auto t_ = t - travel_start_time;
            return travel_start + travel_velocity * t_;
        }
        if(travel_end_time <= t and t < duration) {
            auto t_ = t - travel_end_time;
            return travel_end + travel_velocity * t_ + (instruction.a3 * t_ * t_) / 2;
        }
        return destination;
    }

    /**
     * @brief Compute the velocity of the motion at a given point in time (relative to the start time of the motion).
     */
    double get_velocity(double t) const {
        if(t < 0) return start_velocity;

        if(0 <= t and t < travel_start_time) {
            auto t_ = t;
            return start_velocity + instruction.a1 * t_;
        }
        if(travel_start_time <= t and t < travel_end_time) {
            return travel_velocity;
        }
        if(travel_end_time <= t and t < duration) {
            auto t_ = t - travel_end_time;
            return travel_velocity + instruction.a3 * t_;
        }
        return final_velocity;
    }

};
