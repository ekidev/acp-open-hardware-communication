

#include "Arduino.h"

namespace cycles64
{

    namespace // private -----------------------------
    {
        uint32_t oldLow = ARM_DWT_CYCCNT;
        uint32_t curHigh = 0;
    } // end private namespace <<---------------------

    uint64_t get()
    {
        noInterrupts();
        uint32_t curLow = ARM_DWT_CYCCNT;
        if (curLow < oldLow) // roll over
        {
            curHigh++;
        }
        oldLow = curLow;
        uint64_t curVal = ((uint64_t)curHigh << 32) | curLow;
        interrupts();

        return curVal;
        
    }

    void begin()
    {
        // Enable the ARM_DWT_CYCCNT cycle counter
        ARM_DEMCR |= ARM_DEMCR_TRCENA;
        ARM_DWT_CTRL |= ARM_DWT_CTRL_CYCCNTENA;
    }
}
