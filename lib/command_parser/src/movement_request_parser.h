#pragma once

#include <ArduinoJson.h>

#include "movement_request.h"

/**
 * @brief If the variant is an array, gets the value at the index, otherwise just returns the passed variant.
 */
inline JsonVariant get_value_at(JsonVariant v, size_t idx) {
    if(v.is<JsonArray>()) {
        return v[idx];
    } else {
        return v;
    }
}

/**
 * @brief Checks if a variant is either a double or an int.
 */
inline auto is_numeric(JsonVariant v) {
    return v.is<double>() or v.is<int>();
}

/**
 * @brief Return the number of items in a json array, or infinite, if the passed variant is not an array.
 */
inline auto get_number_of_items(JsonVariant v) {
    if(v.is<JsonArray>()) {
        return v.as<JsonArray>().size();
    }
    if(is_numeric(v)) {
        return std::numeric_limits<size_t>::max();
    }
    if(v.isNull()) {
        return std::numeric_limits<size_t>::max();
    }
    return size_t{0};
}

/**
 * @brief Finds the minimum number of items in a given list of variants.
 *
 * @note If all variants are null, 0 is returned.
 */
inline auto find_min_number_of_items(const std::initializer_list<JsonVariant>& variants) {

    if (std::all_of(variants.begin(), variants.end(), [](const JsonVariant& v) {return v.isNull();})) {
        return size_t{0};
    }

    auto min_num_items = std::numeric_limits<size_t>::max();
    for(auto& variant : variants) {
        min_num_items = std::min(min_num_items, get_number_of_items(variant));
    }
    return min_num_items;
}

/**
 * @brief Parses `MovementRequest`s from a json object.
 *
 * The json object is assumed to contain the keys "destination", "acceleration", "deceleration", "max_vel", "end_vel", "motor_idx" and "duration"
 * with either a value or an array of values. If one of the keys is missing, its value is taken from the default movement request.
 *
 * If one of those keys contains an array, multiple movement requests are parsed.
 * For example the request {"destination": [10, 50], "max_vel": 5} will parse to two movement request:
 * one to move motor 0 to position 10 at velocity 5 and one to move motor 1 to position 50 at velocity 5.
 *
 * If "motor_idx" is not specified, indexes starting from 0 are assumed for each new request.
 *
 * The special key "celeration" allows to set "acceleration" and "deceleration" at the same time and takes precedence over them.
 *
 * @tparam The type of the movement request handler. This template parameter allows to pass anything callable as a handler.
 * @param json The json object to examine for parseable movement requests.
 * @param movement_request_handler Callable A function to be called with each movement request, that could be parsed.
 * The `MovementRequest` is passed to the callable.
 * @param defaults A movement request describing default values that should be assumed whenever a value is missing in the json object.
 */
template<class Callable>
inline void parseMovementRequestsFromJson(JsonObject json, Callable movement_request_handler, const MovementRequest& defaults = {0, 0, 1, 1, 1, 0}) {

    // NOTE: we fetch all the variants here, so we do not fetch them again and again in the loop below.
    auto destination_variant = json["destination"];
    auto acceleration_variant = json["acceleration"];
    auto deceleration_variant = json["deceleration"];
    auto max_vel_variant = json["max_vel"];
    auto end_vel_variant = json["end_vel"];
    auto motor_idx_variant = json["motor_idx"];
    auto duration_variant = json["duration"];

    if (json.containsKey("celeration")) {
        acceleration_variant = json["celeration"];
        deceleration_variant = acceleration_variant;
    }

    /// DEDUCE NUMBER OF REQUESTS WITHIN THE JSON OBJECT
    auto num_requests = find_min_number_of_items({destination_variant, acceleration_variant, deceleration_variant, max_vel_variant, motor_idx_variant, duration_variant});
    if(num_requests == std::numeric_limits<size_t>::max()) num_requests = 1; // TODO: optionally set to NUM_MOTORS instead of 1 here. Then we move ALL motors be default instead of the one with idx 1

    /// PRODUCE MOVEMENT REQUESTS
    for(uint8_t i = 0; i < num_requests; i++) {
        auto motor_idx = get_value_at(motor_idx_variant, i) | i;
        auto destination = get_value_at(destination_variant, i) | defaults.destination;
        auto acceleration = get_value_at(acceleration_variant, i) | defaults.acceleration;
        auto deceleration = get_value_at(deceleration_variant, i) | defaults.deceleration;
        auto max_velocity = get_value_at(max_vel_variant, i) | defaults.max_velocity;
        auto end_velocity = get_value_at(end_vel_variant, i) | defaults.end_velocity;
        auto duration = get_value_at(duration_variant, i) | defaults.duration;

        movement_request_handler(
                MovementRequest{destination, end_velocity, acceleration, deceleration, max_velocity, duration},
                motor_idx
        );
    }
}

/**
 * @brief Parses `MovementRequest`s requests from a Json Array.
 *
 * Each value in the array is assumed to describe the destination.
 * Motor indexes are increasing from 0.
 * All other values of the request are copied from the default value.
 *
 * @tparam The type of the movement request handler. This template parameter allows to pass anything callable as a handler.
 * @param json The json array containing destinations.
 * @param movement_request_handler Callable A function to be called with each movement request, that could be parsed.
 * The `MovementRequest` is passed to the callable.
 * @param defaults A movement request describing the values except for motor index and destination.
 */
template<class Callable>
inline auto parseMovementRequestsFromJson(JsonArray json, Callable movement_request_handler, const MovementRequest& defaults = {0, 0, 1, 1, 1, 0}) {
    for(uint8_t i = 0; i < json.size(); i++) {
        movement_request_handler(
                MovementRequest{json[i] | defaults.destination, defaults.end_velocity, defaults.acceleration, defaults.deceleration, defaults.max_velocity, defaults.duration},
                i);
    }
}

