#pragma once
#include<ArduinoJson.h>
#include<movement_request.h>
#include<limits>

/**
 * @brief Validates movement requests by ensuring all fields are in the given range.
 */
struct MovementRequestValidator {
    MovementRequest min_ = {-10000, 0, 500, 500, 20, 0};
    MovementRequest max_ = {10000, 0, 10000, 10000, 4000, std::numeric_limits<double>::max()};

    auto operator()(const MovementRequest& m_req, JsonArray& errors) const {

        auto ensure_positive = [&](auto val, auto name) {
            if (val < 0) {
                auto error = errors.createNestedObject();
                error["msg"] = "Must be positive";
                error["field"] = name;
                error["value"] = val;
                error["assumed_value"] = -val;
                return -val;
            }
            return val;
        };

        auto ensure_in_range = [&](auto val, auto min, auto max, auto name) -> decltype(val){
                if (val < min) {
                    auto error = errors.createNestedObject();
                    error["msg"] = "Must be in range";
                    error["min"] = min;
                    error["max"] = max;
                    error["field"] = name;
                    error["value"] = val;
                    error["assumed_value"] = min;
                    return min;
                }

                if (max < val) {
                    auto error = errors.createNestedObject();
                    error["msg"] = "Must be in range";
                    error["min"] = min;
                    error["max"] = max;
                    error["field"] = name;
                    error["value"] = val;
                    error["assumed_value"] = max;
                    return max;
                }

                return val;
        };

        auto ensure_positive_and_in_range = [&](auto val, auto min, auto max, auto name) {
            return ensure_in_range(ensure_positive(val, name), min, max, name);
        };

        MovementRequest validated_req = MovementRequest(m_req);

        if(m_req.duration < 0) {
            auto error = errors.createNestedObject();
            error["msg"] = "Must be positive";
            error["field"] = "duration";
            error["value"] = m_req.duration;
            error["assumed_value"] = 0;
            validated_req.duration = 0;
        } else {
            validated_req.duration = m_req.duration;
        }


        validated_req.acceleration = ensure_positive_and_in_range(m_req.acceleration, min_.acceleration, max_.acceleration, "acceleration");
        validated_req.deceleration = ensure_positive_and_in_range(m_req.deceleration, min_.deceleration, max_.deceleration, "deceleration");
        validated_req.max_velocity = ensure_positive_and_in_range(m_req.max_velocity, min_.max_velocity, max_.max_velocity, "max_velocity");
        validated_req.destination = ensure_in_range(m_req.destination, min_.destination, max_.destination, "destination");

        // Note: for the end velocity we ensure that it with the given min/max values but also its magnitude does not
        // exceed the max velocity.
        validated_req.end_velocity = ensure_in_range(m_req.end_velocity,
                                                     std::max(-validated_req.max_velocity, min_.end_velocity),
                                                     std::min(validated_req.max_velocity, max_.end_velocity), "end_velocity");

        return validated_req;
    }
};
