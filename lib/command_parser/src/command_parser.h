/**
 * @file
 */
#pragma once

#include<algorithm>
#include <chrono>

#include<movement_request_parser.h>
#include<movement_request_validator.h>
#include<motor_mover.h>
#include<motion_to_json.h>

/**
 * @tparam NUM_MOTORS The maximal number of motors to parse requests for.
 * @tparam Motion The motion type to use.
 */
template<uint8_t NUM_MOTORS, class Motion, size_t max_queue_length = 200>
class JsonCommandParser {
public:

    auto parseString(const char* request_str,  MotorMover<NUM_MOTORS, Motion>& motor_mover) {
        auto request = StaticJsonDocument<3000>();
        auto error = deserializeJson(request, request_str);

        if (error and error != last_error) {
            last_error = error;
            auto response = StaticJsonDocument<3000>();
            response["errors"][0]["msg"] = "Can not parse input";
            response["errors"][0]["error"] = error.c_str();
            response["errors"][0]["input"] = request_str;
            return response;
        } else {
            last_error = error;
            return parseJsonDocument(request, motor_mover);
        }
    }
    /**
     * @brief Parses movement requests from a json document, that is either an array or a dictionary and keeps track of min, max and default values.
     * 
     * When the document contains the "min", "max" or "default" keys, those values are parsed and stored to be used as min, max and default values for any future requests.
     * 
     * The default request is used to complete any underspecified requests and all requests are clipped between the min and the max request.
     * 
     * @param json The json document to parse.
     * @return A response object to be sent back.
     */
    auto parseJsonDocument(JsonDocument& json, MotorMover<NUM_MOTORS, Motion>& motor_mover) {
        auto now = teensy_clock::now();

        auto response = StaticJsonDocument<3000>();
        auto errors = response.createNestedArray("errors");
        if (not json.is<JsonObject>()) {
            errors.add("Invalid json document");
            return response;
        }

        if (json["request_movement_now"]) {
            if (not json["request_movement_now"].is<JsonObject>()) {
                errors.add("request_movement_now expects a dictionary");
            } else {
                auto handler = [&](const MovementRequest &request, size_t motor_idx) {

                    if (motor_idx >= NUM_MOTORS) {
                        errors.add("Invalid motor index.");
                        return;
                    }
                    auto validated_request = validator_(request, errors);
                    auto motion = motor_mover.request_movement_now(validated_request, motor_idx, now);
                    response["empty_queue_time"] = std::chrono::duration<double>(motor_mover.get_last_motion_end_time(now) - now).count();
                    auto motion_response = response.createNestedObject("requested_motion");
                    convertMotionToJson(motion, motion_response);
                };
                parseMovementRequestsFromJson(json["request_movement_now"].as<JsonObject>(), handler, default_);
            }
            json.remove("request_movement_now");
        }

        if (json["request_next_movement"]) {

            if (not json["request_next_movement"].is<JsonObject>()) {
                errors.add("request_next_movement expects a dictionary");
            } else {
                auto handler = [&](const MovementRequest &request, size_t motor_idx) {
                    if (motor_idx >= NUM_MOTORS) {
                        errors.add("Invalid motor index.");
                        return;
                    }

                    if (motor_mover.longest_queue_size() >= max_queue_length) {
                        errors.add("Queue is full.");
                        return;
                    }
                    auto validated_request = validator_(request, errors);
                    auto motion = motor_mover.request_movement(validated_request, motor_idx, now);
                    response["empty_queue_time"] = std::chrono::duration<double>(motor_mover.get_last_motion_end_time(now) - now).count();
                    auto motion_response = response.createNestedObject("requested_motion");
                    convertMotionToJson(motion, motion_response);
                };
                parseMovementRequestsFromJson(json["request_next_movement"].as<JsonObject>(), handler, default_);
            }
            json.remove("request_next_movement");
        }

        if (json["get"]) {
            if(not json["get"].is<JsonArray>()) {
                errors.add("get expects an array");
            } else {
                for (auto state: json["get"].as<JsonArray>()) {
                    if (not state.is<JsonString>()) {
                        errors.add("get_state expects an array of strings");
                        continue;
                    }
                    if (state == "motor_positions") {
                        auto positions_array = response.createNestedArray("motor_positions");
                        for (size_t i = 0; i < NUM_MOTORS; i++) {
                            positions_array.add(motor_mover.motor_stepper_.get_current_position(i));
                        }
                        continue;
                    }
                    if (state == "number_of_enqueued_motions") {
                        response["number_of_enqueued_motions"] = motor_mover.longest_queue_size();
                        continue;
                    }
                    if (state == "empty_queue_time") {
                        for (size_t i = 0; i < NUM_MOTORS; i++) {
                            response["empty_queue_time"] = std::chrono::duration<double>(motor_mover.get_last_motion_end_time(now) - now).count();
                        }
                        continue;
                    }
                    if (state == "delayed_steps") {
                        response["delayed_steps"] = motor_mover.motor_stepper_.collect_delayed_steps();
                        continue;
                    }
                    auto add_to_response_if_requested = [&](const char* key, auto& value) {
                        if (state == key) {
                            response[key] = value;
                            return true;
                        }
                        return false;
                    };
                    if (add_to_response_if_requested("min_acceleration", validator_.min_.acceleration)) continue;
                    if (add_to_response_if_requested("max_acceleration", validator_.max_.acceleration)) continue;
                    if (add_to_response_if_requested("default_acceleration", default_.acceleration)) continue;
                    if (add_to_response_if_requested("min_deceleration", validator_.min_.deceleration)) continue;
                    if (add_to_response_if_requested("max_deceleration", validator_.max_.deceleration)) continue;
                    if (add_to_response_if_requested("default_deceleration", default_.deceleration)) continue;
                    if (add_to_response_if_requested("min_max_velocity", validator_.min_.max_velocity)) continue;
                    if (add_to_response_if_requested("max_max_velocity", validator_.max_.max_velocity)) continue;
                    if (add_to_response_if_requested("default_max_velocity", default_.max_velocity)) continue;

                    // TODO: also handle power_supply_state and power_button_state
                    JsonObject error = errors.createNestedObject();
                    error["message"] = "Unknown state requested";
                    error["state"] = state;
                    auto possible_states = error.template createNestedArray("possible_states");
                    possible_states.add("motor_positions");
                    possible_states.add("number_of_enqueued_motions");
                    possible_states.add("empty_queue_time");
                    possible_states.add("delayed_steps");
                    possible_states.add("min_acceleration");
                    possible_states.add("max_acceleration");
                    possible_states.add("default_acceleration");
                    possible_states.add("min_deceleration");
                    possible_states.add("max_deceleration");
                    possible_states.add("default_deceleration");
                    possible_states.add("min_max_velocity");
                    possible_states.add("max_max_velocity");
                    possible_states.add("default_max_velocity");
                }
            }
            json.remove("get");
        }
        if(json["set"]) {
            auto set = json["set"];
            if(not set.is<JsonObject>()) {
                errors.add("set expects a dictionary");
            } else {
                auto set_if_match = [&](auto key, JsonVariant value, const char* name, auto& target) {
                    if (key == name) {
                        if(not is_numeric(value)) {
                            errors.add(String(key.c_str()) + " expects a number");
                        } else {
                            target = value;
                            response[key] = value;
                        }
                        return true;
                    }
                    return false;
                };
                for(auto i : set.as<JsonObject>()) {
                    if(set_if_match(i.key(), i.value(), "min_acceleration", validator_.min_.acceleration)) continue;
                    if(set_if_match(i.key(), i.value(), "max_acceleration", validator_.max_.acceleration)) continue;
                    if(set_if_match(i.key(), i.value(), "default_acceleration", default_.acceleration)) continue;
                    if(set_if_match(i.key(), i.value(), "min_deceleration", validator_.min_.deceleration)) continue;
                    if(set_if_match(i.key(), i.value(), "max_deceleration", validator_.max_.deceleration)) continue;
                    if(set_if_match(i.key(), i.value(), "default_deceleration", default_.deceleration)) continue;
                    if(set_if_match(i.key(), i.value(), "min_max_velocity", validator_.min_.max_velocity)) continue;
                    if(set_if_match(i.key(), i.value(), "max_max_velocity", validator_.max_.max_velocity)) continue;
                    if(set_if_match(i.key(), i.value(), "default_max_velocity", default_.max_velocity)) continue;

                    errors.add(String(i.key().c_str()) + " can not be set");
                }
            }
            json.remove("set");
        }

        for(auto i : json.as<JsonObject>()) {
            errors.add(String(i.key().c_str()) + ": unknown command");
        }
        if(errors.size() == 0) {
            response.remove("errors");
        }
        return response;
    }
private:
    MovementRequest default_ = {0, 0, 1000, 1000, 2000, 0};
    MovementRequestValidator validator_;
    DeserializationError last_error;
};
