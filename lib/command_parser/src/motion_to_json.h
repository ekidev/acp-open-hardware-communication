#pragma once

#include<ArduinoJson.h>

template<class Motion>
void convertMotionToJson(const Motion& motion, JsonObject& json) {
    json["destination"] = motion.destination();
    json["acceleration"] = motion.relative_motion_.instruction.a1;
    json["deceleration"] = motion.relative_motion_.instruction.a3;
    json["travel_vel"] = motion.relative_motion_.travel_velocity;
    json["end_vel"] = motion.relative_motion_.final_velocity;
    json["duration"] = motion.relative_motion_.duration;
}