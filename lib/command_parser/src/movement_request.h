#pragma once

/**
 * @brief Describes a request to move a single motor.
 * 
 * @note A duration of zero indicates that the movement should happen as quickly as possible.
 * 
 */
struct MovementRequest {    
    double destination, end_velocity, acceleration, 
    deceleration, max_velocity, duration;
};