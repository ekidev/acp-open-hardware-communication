#include <Arduino.h>
#include <memory>
#include <teensy_clock.h>
#include <command_parser.h>
#include <experimental/optional>
#include <motor_mover.h>


#define PIN_SOLID_STATE_RELAY_STATUS    16

using duration = teensy_clock::duration;
using namespace std::chrono_literals;
template<class T>
using optional = std::experimental::optional<T>;

constexpr auto num_motors = 14;
auto motor_mover = optional<MotorMover<num_motors>>{};

IntervalTimer steppingTimer;

void steppingTimerCallback() {
    motor_mover->loop(teensy_clock::now());
}

void setup() {
  Serial.begin(9600);
  Serial.setTimeout(100);
  while(!Serial);
  teensy_clock::begin();

  
  pinMode(PIN_SOLID_STATE_RELAY_STATUS, OUTPUT);
  digitalWriteFast(PIN_SOLID_STATE_RELAY_STATUS, HIGH); // Enables power to the motors

  motor_mover.emplace();

  // A value of 300ms is a good trade-off between still responding to commands and not losing steps.
  // TODO: couple this to the maximum velocity of the motors.
  steppingTimer.begin(steppingTimerCallback, 300);
}

unsigned int loop_count = 0;


JsonCommandParser<num_motors, AbsoluteMotion<AccelDecelMotion, teensy_clock> > parser;


void loop() {
  while(Serial.available() > 2) {
      auto request = Serial.readStringUntil('\n', 3000);
      auto response = parser.parseString(request.c_str(), *motor_mover);
      serializeJson(response, Serial);
      Serial.println();
  }
  loop_count++;
}