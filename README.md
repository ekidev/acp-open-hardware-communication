This is the firmware for the microcontrollers of the Elias Crespin sculptures.
It receives movement requests, computes optimal movement paths and drives stepper motors according to those optimal paths.

# Implementation Details

## State
The state of the sculpture consists of two groups.

Changes in the first group are immediately broadcast:

- Number of enqueued motions
- Most recently enqueued motion
- Time when current queue will be empty
- Min/Max/Defaults for acceleration and max velocity
- Power supply state/power button state

Changes in the second group need to be explicitely requested (though items from the first group can also be requested).

- Motor Position and Velocity
- All enqueued motions


## API
We use JSON objects to communicate with the sculpture firmware.

### Movement Requests
Movement requests are to be formatted in the following way:
```json
{
  "destination": <destination> or [<destination>, <destination>, ...],
  "acceleration": <acceleration> or [<acceleration>, <acceleration>, ...],
  "deceleration": <deceleration> or [<deceleration>, <deceleration>, ...],
  "celeration", <celeration> or [<celeration>, <celeration>, ...], // overrides acceleration and deceleration
  "max_vel": <max_velocity> or [<max_velocity>, <max_velocity>, ...],
  "end_vel": <end_velocity> or [<end_velocity>, <end_velocity>, ...],
  "motor_idx": <motor_idx> or [<motor_idx>, <motor_idx>, ...],
  "duration": <duration> or [<duration>, <duration>, ...],
}
```
For each field you can specify either a single value or an array of values.
If any of the fields is specified as an array, the movement request applied to more than one motor.
The motors are addressed by their `motor_idx`. 
If it is not specified, increasing motor indices starting from 0 are used.
If any of the other fields is not specified, the default value is used.

For example the following request will move the motors 0, 1 and 2 to the same destination
with different maximum speeds.
```json
{
  "destination": 5,
  "max_vel": [1, 2, 3]
}
```

This request will move the motors 5, 8 and 10 to different destinations:
```json
{
  "motor_idx": [5, 8, 10],
  "destination": [1, 2, 3]
}
```


### Request Movement Now
1. Validate Movement
2. Replace current motions with a new request 
If the requested movement ends in a nonzero velocity and there is no next movement, also add a next movement to the requested destination with zero velocity.

```json
{"request_movement_now":  <movement_request>}
```

### Request Next Movement
1. Validate Movement
2. Adds the movement to the queue

```json
{"request_next_movement":  <movement_request>}
```

### Get State
Request any element of the state, which can be anything described in the above section

```json
{"get": [<state_element>, <state_element>, ...]}
```
Where <state_element> is one of the following:

- `motor_positions`
- `number_of_enqueued_motions`
- `most_recent_enqueued_motion`
- `empty_queue_time`
- `delayed_steps`
- `min_acceleration`, `max_acceleration`, `default_acceleration`
- `min_deceleration`, `max_deceleration`, `default_deceleration`
- `min_max_velocity`, `max_max_velocity`, `default_max_velocity`
- `power_supply_state`, `power_button_state`

The answer will be of the format:
```json
{
  <state_element>: <state_element_value>,
  <state_element>: <state_element_value>,
  ...
}
```

### Set Parsing min/max/default

Ti set any of the parsing parameters, send a request of the following format:
```json
{"set": {
  "min_acceleration": <min_acceleration>,
  "max_acceleration": <max_acceleration>,
  "default_acceleration": <default_acceleration>,
  "min_deceleration": <min_deceleration>,
  "max_deceleration": <max_deceleration>,
  "default_deceleration": <default_deceleration>,
  "min_max_velocity": <min_max_velocity>,
  "max_max_velocity": <max_max_velocity>,
  "default_max_velocity": <default_max_velocity>
}
}
```
Each of the fields is optional.

### Emergency Stop

