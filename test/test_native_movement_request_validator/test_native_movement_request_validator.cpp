#include<unity.h>
#include "movement_request_validator.h"
#include "random_movement_request.h"

void test_acceleration_and_velocity_and_destination_bounds_ensured() {

    auto validator = MovementRequestValidator{
        MovementRequest{-50, 5, 7, 8, 6, 0},
        MovementRequest{50, 5, 8, 9, 8, 3}
    };

    for(int i = 0; i < 10000; i++) {
        auto r = random_movement_request(10);
        auto errors = JsonArray();
        auto validated_r = validator(r, errors);

        TEST_ASSERT_GREATER_OR_EQUAL(validator.min_.acceleration, validated_r.acceleration);
        TEST_ASSERT_LESS_OR_EQUAL(validator.max_.acceleration, validated_r.acceleration);

        TEST_ASSERT_GREATER_OR_EQUAL(validator.min_.deceleration, validated_r.deceleration);
        TEST_ASSERT_LESS_OR_EQUAL(validator.max_.deceleration, validated_r.deceleration);

        TEST_ASSERT_GREATER_OR_EQUAL(validator.min_.max_velocity, validated_r.max_velocity);
        TEST_ASSERT_LESS_OR_EQUAL(validator.max_.max_velocity, validated_r.max_velocity);

        TEST_ASSERT_GREATER_OR_EQUAL(validator.min_.destination, validated_r.destination);
        TEST_ASSERT_LESS_OR_EQUAL(validator.max_.destination, validated_r.destination);
    }
}

void test_acceleration_bounds_violations_are_reported() {
    auto validator = MovementRequestValidator{
            MovementRequest{-50, 5, 7, 8, 6, 0},
            MovementRequest{50, 6, 8, 9, 8, 3}
    };
    auto r = MovementRequest{0, 5, 12, 8, 7, 0};
    auto errors_doc = StaticJsonDocument<5000>();
    auto errors = errors_doc.to<JsonArray>();
    auto validated_r = validator(r, errors);
    TEST_ASSERT_EQUAL(1, errors.size());
    TEST_ASSERT_TRUE(errors[0]["field"] == "acceleration");
    TEST_ASSERT_EQUAL(errors[0]["value"].as<double>(), r.acceleration);
    TEST_ASSERT_EQUAL(errors[0]["min"].as<double>(), validator.min_.acceleration);
    TEST_ASSERT_EQUAL(errors[0]["max"].as<double>(), validator.max_.acceleration);
}

void test_velocity_bounds_violations_are_reported() {
    auto validator = MovementRequestValidator{
            MovementRequest{-50, 5, 7, 8, 6, 0},
            MovementRequest{50, 6, 8, 9, 8, 3}
    };
    auto r = MovementRequest{0, 5, 7.5, 8, 3, 0};
    auto errors_doc = StaticJsonDocument<5000>();
    auto errors = errors_doc.to<JsonArray>();
    auto validated_r = validator(r, errors);
    TEST_ASSERT_EQUAL(1, errors.size());
    TEST_ASSERT_TRUE(errors[0]["field"] == "max_velocity");
    TEST_ASSERT_EQUAL(errors[0]["value"].as<double>(), r.max_velocity);
    TEST_ASSERT_EQUAL(errors[0]["min"].as<double>(), validator.min_.max_velocity);
    TEST_ASSERT_EQUAL(errors[0]["max"].as<double>(), validator.max_.max_velocity);
}

void test_destination_bounds_violations_are_reported() {
    auto validator = MovementRequestValidator{
            MovementRequest{-50, 5, 7, 8, 6, 0},
            MovementRequest{50, 6, 8, 9, 8, 3}
    };
    auto r = MovementRequest{5000, 5, 7.5, 8, 7, 0};
    auto errors_doc = StaticJsonDocument<5000>();
    auto errors = errors_doc.to<JsonArray>();
    auto validated_r = validator(r, errors);
    TEST_ASSERT_EQUAL(1, errors.size());
    TEST_ASSERT_TRUE(errors[0]["field"] == "destination");
    TEST_ASSERT_EQUAL(errors[0]["value"].as<double>(), r.destination);
    TEST_ASSERT_EQUAL(errors[0]["min"].as<double>(), validator.min_.destination);
    TEST_ASSERT_EQUAL(errors[0]["max"].as<double>(), validator.max_.destination);
}

void test_end_velocity_bounds_violations_are_reported() {
    auto validator = MovementRequestValidator{
            MovementRequest{-50, 5, 7, 8, 6, 0},
            MovementRequest{50, 6, 8, 9, 8, 3}
    };
    auto r = MovementRequest{5, 7, 7.5, 8, 7, 0};
    auto errors_doc = StaticJsonDocument<5000>();
    auto errors = errors_doc.to<JsonArray>();
    auto validated_r = validator(r, errors);
    TEST_ASSERT_EQUAL(1, errors.size());
    TEST_ASSERT_TRUE(errors[0]["field"] == "end_velocity");
    TEST_ASSERT_EQUAL(errors[0]["value"].as<double>(), r.end_velocity);
    TEST_ASSERT_EQUAL(errors[0]["min"].as<double>(), validator.min_.end_velocity);
    TEST_ASSERT_EQUAL(errors[0]["max"].as<double>(), validator.max_.end_velocity);
}


int main() {
    UNITY_BEGIN();
    RUN_TEST(test_acceleration_and_velocity_and_destination_bounds_ensured);
    RUN_TEST(test_acceleration_bounds_violations_are_reported);
    RUN_TEST(test_velocity_bounds_violations_are_reported);
    RUN_TEST(test_destination_bounds_violations_are_reported);
    RUN_TEST(test_end_velocity_bounds_violations_are_reported);
    UNITY_END();
}