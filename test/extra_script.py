# Note this script is needed as a workaround for this issue: https://community.platformio.org/t/teensy-4-1-unit-testing-issue/21033
Import("env")

def after_upload(source, target, env):
    print("Delay while uploading...", end="")
    import time
    time.sleep(0.5)
    print("Done!")

env.AddPostAction("upload", after_upload)