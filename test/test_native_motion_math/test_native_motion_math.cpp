#include<string>
#include"movement_instruction.h"
#include"unity.h"

// Note: if the includes below are not found, you have to generate them by executing `pytest test_motion_instruction.py` in the `motion_math` folder.
#include<example_calls_0.h>
#include<example_calls_1.h>
#include<example_calls_2.h>
#include<example_calls_3.h>
#include<example_calls_4.h>
#include<example_calls_5.h>
#include<example_calls_6.h>
#include<example_calls_7.h>
#include<example_calls_8.h>
#include<example_calls_9.h>

int main() {
    UNITY_BEGIN();
    RUN_TEST(run_examples_0);
    RUN_TEST(run_examples_1);
    RUN_TEST(run_examples_2);
    RUN_TEST(run_examples_3);
    RUN_TEST(run_examples_4);
    RUN_TEST(run_examples_5);
    RUN_TEST(run_examples_6);
    RUN_TEST(run_examples_7);
    RUN_TEST(run_examples_8);
    RUN_TEST(run_examples_9);
    UNITY_END();
}