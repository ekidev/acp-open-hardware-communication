#include<movement_instruction.h>
#include<Arduino.h>
#include<unity.h>

void test_motion_instructionso_speed() {
    
    auto start = micros();
    for(size_t i = 0; i < 100; i++) {
        compute_movement_instruction(100, 0, 0, 10, 10, 10, 20);
    }
    auto duration = (micros() - start)/100.;

    
    TEST_MESSAGE((String("It takes ") + String(duration) + String("us per computation!")).c_str());
    TEST_ASSERT_FLOAT_WITHIN(10, 500, duration);
}

void setup() {
    delay(2000);
    UNITY_BEGIN();
    RUN_TEST(test_motion_instructionso_speed);
    UNITY_END();
}

void loop() {}