#include<string>
#include<chrono>
#include<vector>
#include<algorithm>

#include<unity.h>
#include<ArduinoJson.h>
#include<movement_request_parser.h>

using time_point = std::chrono::system_clock::time_point;


void get_value_at_with_single_value() {
    auto json = StaticJsonDocument<3000>();
    auto error = deserializeJson(json, "{\"position\": 5 }");
    TEST_ASSERT_FALSE(error);

    for(int i = 0; i < 100; i++)
        TEST_ASSERT_EQUAL(get_value_at(json["position"], i), 5);
}

void get_value_at_with_multiple_values() {
    auto json = StaticJsonDocument<3000>();
    auto error = deserializeJson(json, "{\"position\": [1, 2, 3, 4, 5] }");
    TEST_ASSERT_FALSE(error);    

    for(int i = 0; i < 5; i++)
        TEST_ASSERT_EQUAL(get_value_at(json["position"], i), i+1);
}

void num_items_with_single_value() {
    auto json = StaticJsonDocument<3000>();
    auto error = deserializeJson(json, "{\"position\": 5 }");
    TEST_ASSERT_FALSE(error);

    TEST_ASSERT_EQUAL(get_number_of_items(json["position"]), std::numeric_limits<size_t>::max());
}

void num_items_with_multiple_values() {
    auto json = StaticJsonDocument<3000>();
    auto error = deserializeJson(json, "{\"position\": [1, 2, 3, 4, 5] }");
    TEST_ASSERT_FALSE(error);

    TEST_ASSERT_EQUAL(get_number_of_items(json["position"]), 5);
}

void assert_movement_requests_equal(const MovementRequest& lhs, const MovementRequest& rhs) {
    TEST_ASSERT_EQUAL(lhs.destination, rhs.destination);
    TEST_ASSERT_EQUAL(lhs.end_velocity, rhs.end_velocity);
    TEST_ASSERT_EQUAL(lhs.acceleration, rhs.acceleration);
    TEST_ASSERT_EQUAL(lhs.deceleration, rhs.deceleration);
    TEST_ASSERT_EQUAL(lhs.max_velocity, rhs.max_velocity);
    TEST_ASSERT_EQUAL(lhs.duration, rhs.duration);
}

void assert_json_parses_to_movement_requests(const char* json_string,
                                             const std::vector<MovementRequest>& movement_requests,
                                             const std::vector<size_t>& motor_indices) {
    std::vector<MovementRequest> parsed_movement_requests;
    std::vector<size_t> parsed_motor_idxs;
    auto handler = [&](const MovementRequest& r, size_t motor_idx) {
        parsed_movement_requests.push_back(r);
        parsed_motor_idxs.push_back(motor_idx);
    };

    auto json = StaticJsonDocument<3000>();
    deserializeJson(json, json_string);

    if(json.is<JsonArray>()) {
        parseMovementRequestsFromJson(json.as<JsonArray>(), handler);
    } else if (json.is<JsonObject>()) {
        parseMovementRequestsFromJson(json.as<JsonObject>(), handler);
    } else {
        TEST_FAIL_MESSAGE("json is not an array or object");
    }


    TEST_ASSERT_EQUAL_MESSAGE(movement_requests.size(), parsed_movement_requests.size(), "Wrong number of movement requests.");

    for(size_t i = 0; i < movement_requests.size(); i++) {
        assert_movement_requests_equal(movement_requests[i], parsed_movement_requests[i]);
        TEST_ASSERT_EQUAL(parsed_motor_idxs[i], motor_indices[i]);
    }

}

void test_is_numeric() {
    auto json = StaticJsonDocument<3000>();
    auto error = deserializeJson(json, R"(
    {
        'destination': 42,
        'celeration': 50.,
        'end_vel': -10,
        'max_vel': 7.,
        'duration': 9,
        'some_string': 'hi there',
        'some_array': ['a', 1],
        'some_dict': {'a': 1}
    }
    )");
    TEST_ASSERT_FALSE_MESSAGE(error, error.c_str());
    TEST_ASSERT_TRUE(json.is<JsonObject>());

    TEST_ASSERT_TRUE(is_numeric(json["destination"]));
    TEST_ASSERT_TRUE(is_numeric(json["celeration"]));
    TEST_ASSERT_TRUE(is_numeric(json["end_vel"]));
    TEST_ASSERT_TRUE(is_numeric(json["max_vel"]));
    TEST_ASSERT_TRUE(is_numeric(json["duration"]));

    TEST_ASSERT_FALSE(is_numeric(json["asd"]));
    TEST_ASSERT_FALSE(is_numeric(json["some_string"]));
    TEST_ASSERT_FALSE(is_numeric(json["some_array"]));
    TEST_ASSERT_FALSE(is_numeric(json["some_dict"]));
}

void test_parsing_single_request() {
    assert_json_parses_to_movement_requests(R"(
    {
        'destination': 42, 
        'celeration': 50.,
        'end_vel': -10,
        'max_vel': 7.,
        'duration': 9
    }
    )",
    {
        {42, -10, 50., 50., 7., 9.},
    },
    {0});
}

void test_parsing_multiple_requests_due_to_multiple_motor_idxs() {
    assert_json_parses_to_movement_requests(R"(
    {
        'motor_idx': [5, 7, 9, 10],
        'destination': 42, 
        'acceleration': 50.,
        'end_vel': -10,
        'max_vel': 7.,
        'duration': 9
    }
    )",
    {
        {42, -10, 50., 1., 7., 9.0},
        {42, -10, 50., 1., 7., 9.0},
        {42, -10, 50., 1., 7., 9.0},
        {42, -10, 50., 1., 7., 9.0},
    },
{5, 7, 9, 10});
}

void test_parsing_multiple_requests_due_to_multiple_destinations() {
    assert_json_parses_to_movement_requests(R"(
    {
        'destination': [10, 20], 
        'celeration': 50., 
        'max_vel': 7.
    }
    )",
    {
        {10, 0, 50., 50., 7., 0.0},
        {20, 0, 50., 50., 7., 0.0}
    },
    {0, 1});
}

void test_parsing_empty_request_results_in_no_request() {
    assert_json_parses_to_movement_requests(R"(
    {
        
    }
    )",
    {},{0});
}

void test_parsing_multiple_requests_due_to_multiple_decelerations() {
    assert_json_parses_to_movement_requests(R"(
    {
        'deceleration': [7., 7., 7.]
    }
    )", 
    {
        {0, 0, 1, 7, 1, 0},
        {0, 0, 1, 7, 1, 0},
        {0, 0, 1, 7, 1, 0}
    },
    {0, 1, 2});
}

void test_parsing_two_requests_when_conflicting_number_of_destinations_and_max_velocities() {
    assert_json_parses_to_movement_requests(R"(
    {
        'destination': [10, 20], 
        'acceleration': 50.,
        'deceleration': -10,
        'max_vel': [10, 110, 1110]
    }
    )", 
    {
        {10, 0, 50., -10., 10., 0.0},
        {20, 0, 50., -10., 110., 0.01}
    },
    {0, 1});
}

void test_parsing_two_requests_when_conflicting_number_of_destinations_and_accelerations() {
    assert_json_parses_to_movement_requests(R"(
    {
        'destination': [10, 20], 
        'acceleration': [50., 51, 53],
        'deceleration': -10,
        'celeration': 42
    }
    )", 
    {
        {10, 0, 42., 42., 1., 0.0},
        {20, 0, 42., 42., 1., 0.0}
    },
    {0, 1});
}

void test_parsing_requests_from_array() {
    assert_json_parses_to_movement_requests(R"(
    [5, 6, 7]
    )", 
    {
        {5, 0, 1, 1, 1, 0},
        {6, 0, 1, 1, 1, 0},
        {7, 0, 1, 1, 1, 0}
    },
    {0, 1, 2});
}

void test_invalid_decerlations_are_replaced_with_default() {
    assert_json_parses_to_movement_requests(R"(
    {
        'deceleration': [7., 'a', 7.]
    }
    )",
    {
        {0, 0, 1, 7, 1, 0},
        {0, 0, 1, 1, 1, 0},
        {0, 0, 1, 7, 1, 0}
    },
    {0, 1, 2});
}

void test_invalid_field_is_ignored() {
    assert_json_parses_to_movement_requests(R"(
    {
        'random stuff': 5
    }
    )",
    {},{0});
}

void test_invalid_specification_of_destination_is_ignored() {
    assert_json_parses_to_movement_requests(R"(
    {
        'destination': {'a': 5}
    }
    )", {}, {0});
}

void test_dict_destination_is_replaced_with_default() {
    assert_json_parses_to_movement_requests(R"(
    {
        'destination': [{'a': 5}, 77],
        'end_vel': 5
    }
    )", 
    {
        {0, 5, 1, 1, 1, 0},
        {77, 5, 1, 1, 1, 0},
    },
    {0, 1});
}

void test_parsing_empty_array_results_in_no_requests() {
    assert_json_parses_to_movement_requests(R"(
    []
    )",
    {}, {});
}

int main() {
    UNITY_BEGIN();
    RUN_TEST(test_is_numeric);
    RUN_TEST(get_value_at_with_single_value);
    RUN_TEST(get_value_at_with_multiple_values);
    RUN_TEST(num_items_with_single_value);
    RUN_TEST(num_items_with_multiple_values);
    RUN_TEST(test_parsing_single_request);
    RUN_TEST(test_parsing_multiple_requests_due_to_multiple_motor_idxs);
    RUN_TEST(test_parsing_multiple_requests_due_to_multiple_destinations);
    RUN_TEST(test_parsing_empty_request_results_in_no_request);
    RUN_TEST(test_parsing_multiple_requests_due_to_multiple_decelerations);
    RUN_TEST(test_parsing_two_requests_when_conflicting_number_of_destinations_and_max_velocities);
    RUN_TEST(test_parsing_two_requests_when_conflicting_number_of_destinations_and_accelerations);
    RUN_TEST(test_parsing_requests_from_array);
    RUN_TEST(test_invalid_decerlations_are_replaced_with_default);
    RUN_TEST(test_invalid_field_is_ignored);
    RUN_TEST(test_invalid_specification_of_destination_is_ignored);
    RUN_TEST(test_dict_destination_is_replaced_with_default);
    RUN_TEST(test_parsing_empty_array_results_in_no_requests);
    RUN_TEST(test_parsing_empty_array_results_in_no_requests);
    UNITY_END();
}