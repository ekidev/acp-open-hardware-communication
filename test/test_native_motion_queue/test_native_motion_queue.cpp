//
// Created by m on 8/19/22.
//

#include <iostream>
#include <fstream>
#include <chrono>

#include <unity.h>

#include <absolute_motion.h>
#include <motion_queue.h>
#include <movement_request.h>
#include <accel_decel_motion.h>
#include <random_movement_request.h>

using namespace std::chrono_literals;




void test_that_velocity_stays_below_max_velocity() {
    // GIVEN
    // a motion queue
    auto q = MotionQueue<AbsoluteMotion<AccelDecelMotion, std::chrono::system_clock> >();
    auto now = std::chrono::system_clock::now();
    auto max_max_vel = 10;
    const auto delta_t = 1ms;  // we check the velocity every delta_t milliseconds
    const auto delta_t_in_s = std::chrono::duration_cast<std::chrono::duration<double>>(delta_t).count();

    // WHEN
    // enqueuing 100 motion requests
    for(int i = 0; i < 100; i++) {
        auto movement_request = random_movement_request(max_max_vel);
        q.request_movement(movement_request, 0, now);
    }

    // THEN
    // the velocity should be below the max velocity at any time
//    std::ofstream file;
//    file.open("test.txt");
    auto motion = q.advance_to(now);
    auto pos_prev = (*motion)(now);
    for(auto t = now+delta_t;
        motion;  // we keep checking the velocity until the motion is finished
        t += delta_t, motion = q.advance_to(t)) {
        auto pos = (*motion)(t);
        auto vel = (pos - pos_prev) / delta_t_in_s;
        TEST_ASSERT_LESS_OR_EQUAL(max_max_vel, std::abs(vel));
//        file << std::chrono::duration<double>(t - now).count() << " " << pos << " " << vel << std::endl;
        pos_prev = pos;
    }

//    file.close();
}

/*
 In this test we first start a movement to 10, then we interrupt it with a new movement to -10 and a terminal velocity of 5.
 This cases a slight overshoot at the end of the movement.
 We test, that at any given time the velocity is below the max velocity.
 Below you can see a visualization of the trajectory and its velocity.

                                           | Here we interrupted the motion to move to -10 instead of 10.
                                           v
   5 :  ...................................:.....................................:......................................:...................##................:....................position *******-:
     :                                     :                                     :                                      :                  #####              :                    velocity ####### :
     :                                     :****************                     :                                      :                ###   ###            :                                     :
     :                                  *****              *****                 :                                      :              ###       ##           :                                     :
     :                              *****  :                   ***               :                                      :             ##          ###         :                                     :
     :                          *****      :                     ***             :                                      :           ###             ###       :                                     :
     :                       ****          :                       ***           :                                      :         ###                 ##      :                                     :
     :      ##################################                       ***         :                                      :        ##                    ###    :                                     :
     :    ###        *****                 : ##                        ***       :                                      :      ###                       ###  :                                     :
     :  ###      *****                     :   ##                        **      :                                      :    ###                           ###:                                     :
     : ##    *****                         :    ###                       ***    :                                      :  ###                               ##                                     :
   0 ###******.............................:......###.......................**...:......................................:.##..................................###.................................+-:
     :                                     :        ##                       **  :                                      ###                                   : ###                      ###        :
     :                                     :         ###                      ** :                                    ###                                     :   ##                    ##          :
     :                                     :           ###                     **:                                   ## :                                     :    ###                ###           :
     :                                     :             ##                     ***                                ###  :                                     :      ###            ###             :
     :                                     :              ###                    :**                             ###    :                                     :        ##          ##               :
     :                                     :                ###                  : **                           ##      :                                     :         ###      ##                 :
     :                                     :                  ##                 :  **                        ###       :                                     :           ###  ###                  :
     :                                     :                   ###               :   **                     ###         :                                     :             ####                    :
     :                                     :                     ###             :    **                   ##           :                                     :                                     :
     :                                     :                       ###           :     *                 ##             :                                     :                                     :
     :                                     :                         ##          :     **              ###              :                                     :                                     :
  -5 :  ...................................:..........................###........:......**...........###................:.....................................:...................................+-:
     :                                     :                            ###      :       **         ##                  :                                     :                                     :
     :                                     :                              ##     :        ***     ###                   :                                     :                                     :
     :                                     :                               ###   :          **  ###                     :               here we overshoot -10 since the terminal velocity is 5 and not 0
     :                                     :                                 ### :           **##                       :                                     :                                     :
     :                                     :                                   ##:           ###                        :                                    *******                                :
     :                                     :                                    ###        ### **                       :                              *******:    ******                           :
     :                                     :                                     :###     ##    **                      :                           ****      :          ****                       :
     :                                     :                                     :  ### ###      ***                    :                         ***         :             ***                     :
     :                                     :                                     :    ###          **                   :                       ***           :               ****                  :
     :                                     :                                     :                  ***                 :                     ***             :                  *****              :
 -10 :  ...................................:.....................................:....................***...............:...................***...............:......................*******......+-:
     :                                     :                                     :                      **              :                 ***                 :                                     :
     :                                     :                                     :                       ****           :               ***                   :                                     :
     :                                     :                                     :                          ***         :             ***                     :                                     :
     :                                     :                                     :                            ****      :         *****                       :                                     :
     :                                     :                                     :                               *******:    ******                           :                                     :
     :                                     :                                     :                                     *******                                :                                     :
     :                                     :                                     :                                      :                                     :                                     :

     0                                     2                                     4                                      6                                     8                                     10
                                                                                                  time
 */
void test_that_velocity_stays_below_max_velocity_when_motion_is_interrupted() {
    // GIVEN
    // a motion queue
    auto q = MotionQueue<AbsoluteMotion<AccelDecelMotion, std::chrono::system_clock> >();
    auto now = std::chrono::system_clock::now();
    auto time_when_interrupted = now + 2s;
    auto max_max_vel = 10.0;
    const auto delta_t = 1ms;  // we check the velocity every delta_t milliseconds
    const auto delta_t_in_s = std::chrono::duration_cast<std::chrono::duration<double>>(delta_t).count();

    auto movement_request = MovementRequest{
            10,
            0,
            5,
            5,
            max_max_vel,
            5};

    auto interrupting_movement_request = MovementRequest{
            -10,
            5,
            5,
            5,
            max_max_vel,
            5};

    // WHEN
    q.request_movement(movement_request, 0, now);

    // THEN
    auto did_interrupt = false;
//    std::ofstream file;
//    file.open("test.txt");

    auto motion = q.advance_to(now);
    auto pos_prev = (*motion)(now);
    for(auto t = now+delta_t;
        motion;  // we keep checking the velocity until the motion is finished
        t += delta_t, motion = q.advance_to(t)) {
        auto pos = (*motion)(t);
        auto vel = (pos - pos_prev) / delta_t_in_s;
        TEST_ASSERT_LESS_OR_EQUAL(max_max_vel, std::abs(vel));
//        file << std::chrono::duration<double>(t - now).count() << " " << pos << " " << vel << std::endl;
        pos_prev = pos;

        if(t >= time_when_interrupted and not did_interrupt) {
            // WHEN
            q.request_movement_now(interrupting_movement_request, pos, t);
            did_interrupt = true;
        }
    }
}

int main() {
    UNITY_BEGIN();
    RUN_TEST(test_that_velocity_stays_below_max_velocity);
    RUN_TEST(test_that_velocity_stays_below_max_velocity_when_motion_is_interrupted);
    UNITY_END();
}